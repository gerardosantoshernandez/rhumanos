package com.crenx.br;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Perfil;
import com.crenx.data.domain.entity.ReglaCredito;
import com.crenx.data.domain.entity.ReglaGasto;
import com.crenx.data.domain.entity.Usuario;
import com.crenx.data.domain.repository.ReglaGastoRepository;
import com.crenx.data.domain.vo.CreditoBRVO;
import com.crenx.data.domain.vo.FondeoVO;
import com.crenx.data.domain.vo.ReglaGastoBRVO;
import com.crenx.security.UserAuthentication;
import com.crenx.services.CatalogoServices;

@Service
public class CatalogoBRImpl {
		
	@Autowired
	CatalogoServices catServices;
	@Autowired
	ReglaGastoRepository regGasRepository;
	
	public List<String> filtraItems(List<String> roles, String tipoCat) 
	{
		ArrayList<String> catItems = new ArrayList<String>();
		if (roles.contains("ROL_ADMIN")||
				roles.contains("ROL_ADMIN_PARAM")||
				roles.contains("ROL_CORPORATIVO")||
				roles.contains("ROL_SOPORTE_OP"))
		{
			catItems.addAll(catServices.getCatalogoIds(tipoCat));			
		}else
		{
			List<String> items = catServices.getCatalogoIds(tipoCat);
			List<ReglaGasto> reglas = regGasRepository.findByRolLlaveNegocioInAndGastoIdCatalogoIn(roles, items);
			if (reglas.size()==0)
			{
				//No hay reglas, regresa todos
				catItems.addAll(catServices.getCatalogoIds(tipoCat));							
			}
			for (ReglaGasto oneRule : reglas)
			{
				catItems.add(oneRule.getGasto().getIdCatalogo());
			}
		}
		if (catItems.size()==0)
			catItems=null;
		return catItems;		
	}
	
	public ReglaGastoBRVO estatusMovCajaCredito(FondeoVO fondeo, UserAuthentication profile)
	{	
		ReglaGastoBRVO result = new ReglaGastoBRVO();
		result.setCrearGasto(true);
		List<String> roles = profile.getDetails().getPerfil().getRoles();
		List<ReglaGasto> reglas = regGasRepository.findByRolLlaveNegocioInAndGastoIdCatalogo(roles, fondeo.getIdTipoGasto());
		if (reglas.size()==0)
		{
			Catalogo status = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
			result.setCorreccion(false);
			result.setEstatusGasto(status);
			return result;
		}
		double montoMaximo=0;
		for (ReglaGasto oneRule: reglas)
		{
			montoMaximo = montoMaximo>oneRule.getMaximo()?montoMaximo:oneRule.getMaximo();
		}
		if (fondeo.getMonto()>montoMaximo)
		{
			Catalogo status = catServices.getCatalogoCveInterna("ESTATUS_GPENDIENTE");
			result.setCorreccion(true);
			result.setEstatusGasto(status);			
		}else
		{
			Catalogo status = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
			result.setCorreccion(false);
			result.setEstatusGasto(status);			
		}						
		return result;
	}
	
	
}

