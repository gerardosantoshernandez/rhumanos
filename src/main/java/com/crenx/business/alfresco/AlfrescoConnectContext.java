package com.crenx.business.alfresco;

/**
 * Contexto utilizado para poder obtener una Session de Alfresco
 * 
 * @author JCHR
 *
 */
public class AlfrescoConnectContext {

	private String username;
	private String pwd;
	private String host;
	private int port;

	/**
	 * Contructor con los datos básicos para conectar
	 */
	public AlfrescoConnectContext(final String username, final String pwd, final String host, final int port) {

		this.port = port;
		this.host = host;
		this.username = username;
		this.pwd = pwd;
	}

	/**
	 * @return
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return
	 */
	public String getPwd() {
		return pwd;
	}

	/**
	 * @param pwd
	 */
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	/**
	 * @return
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port
	 */
	public void setPort(int port) {
		this.port = port;
	}

}
