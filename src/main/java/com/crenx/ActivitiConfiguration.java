package com.crenx;
import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.spring.ProcessEngineFactoryBean;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.crenx.security.activity.GroupManagerFactory;
import com.crenx.security.activity.UserManagerFactory;

@Configuration
public class ActivitiConfiguration {

    @Bean
    public ProcessEngine processEngine(ProcessEngineConfigurationImpl pec, ApplicationContext applicationContext) throws Exception {
        ProcessEngineFactoryBean pe = new ProcessEngineFactoryBean();
        pe.setProcessEngineConfiguration(pec);
        pe.setApplicationContext(applicationContext);

        return pe.getObject();
    }

/*
    @Bean
    public ProcessEngineConfigurationImpl getProcessEngineConfiguration(
            DataSource dataSource,
            PlatformTransactionManager transactionManager,
            ApplicationContext context) {
        SpringProcessEngineConfiguration pec = new SpringProcessEngineConfiguration();

        pec.setDataSource(dataSource);
        pec.setDatabaseSchemaUpdate("true");
        pec.setJobExecutorActivate(true);
        pec.setHistory("full");
        pec.setMailServerPort(2025);
        pec.setDatabaseType("mysql");

        pec.setTransactionManager(transactionManager);
        pec.setApplicationContext(context);

        return pec;
    }
*/
    @Bean
    public RuntimeService getRuntimeService(ProcessEngine processEngine) {
        return processEngine.getRuntimeService();
    }
    
    @Bean
    public BeanPostProcessor activitiConfigurer() {
        return new BeanPostProcessor() {
 
            @Override
            public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
                if (bean instanceof SpringProcessEngineConfiguration) {
                    // If it is the SpringProcessEngineConfiguration, we want to add some extra configuration.
                    SpringProcessEngineConfiguration config = (SpringProcessEngineConfiguration) bean;
                    List<SessionFactory> lista = new ArrayList<SessionFactory>();
                    lista.add(new GroupManagerFactory());
                    lista.add(new UserManagerFactory());                  
                    config.setCustomSessionFactories(lista);
                }
                return bean;
            }
 
            @Override
            public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
                return bean;
            }
        };
    }    
    
    
    
}