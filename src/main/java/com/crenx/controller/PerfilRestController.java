package com.crenx.controller;

import java.security.Principal;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.vo.CrearPerfilVO;
import com.crenx.data.domain.vo.CrearUserVO;
import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.FiltroUsuarioVO;
import com.crenx.services.SeguridadServices;

@RestController
@RequestMapping("/api/perfil")
@Transactional
public class PerfilRestController {
	
	@Autowired
	private SeguridadServices secServices;
	
	
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> getUsuarios(Principal principal,@RequestBody FiltroUsuarioVO filtros) {
		return new ResponseEntity<>(secServices.filtraEmpleados(filtros), HttpStatus.CREATED);
	}

	@RequestMapping(value="savePerfilEmpleado", method = RequestMethod.POST)
	ResponseEntity<?> createPerfilEmpleado(@RequestBody CrearPerfilVO perfil) throws Exception {		
		try
		{
			perfil = secServices.savePerfil(perfil);
			return new ResponseEntity<>(perfil, HttpStatus.CREATED);			
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}
	}
}
