package com.crenx.services;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import com.crenx.business.FileManager;
import com.crenx.data.domain.entity.Documento;
import com.crenx.data.domain.entity.Producto;
import com.crenx.data.domain.entity.PropiedadesDoc;
import com.crenx.data.domain.repository.ProductoRepository;
import com.crenx.data.domain.vo.FiltrosProductoVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.crenx.data.domain.vo.ProductoToVOModelMap;
import com.crenx.data.domain.vo.ProductoVO;
import com.crenx.util.Constantes;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

@Service
public class ProductoServices {
	@Autowired
	private ProductoRepository productoRepository;

	public Collection<ProductoVO> filtrarProductos (FiltrosProductoVO filtros)
	{
		Collection<Producto> productos = null;
		
		String nombre = StringUtils.isEmpty(filtros.getNombre())?"%":filtros.getNombre()+"%";
		List<String> familias = Lists.newArrayList(filtros.getFamilias());
		
		if (filtros.getFamilias().size()>0)
		{
			productos = Lists.newArrayList(this.productoRepository.findByFamiliaProducto_IdCatalogoInAndNombre(filtros.getFamilias(), filtros.getNombre()));
		}else {
			productos = Lists.newArrayList(this.productoRepository.findByNombreIsLike(nombre));
		}
		
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ProductoToVOModelMap());
		Type listType = new TypeToken<List<ProductoVO>>() {}.getType();
		List<ProductoVO> listaProductos = mapper.map(productos, listType);
		return listaProductos;
	}
}
