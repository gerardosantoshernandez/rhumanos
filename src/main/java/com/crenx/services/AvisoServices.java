package com.crenx.services;

import java.util.Collection;
import java.util.List;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.crenx.data.domain.entity.Aviso;
import com.crenx.data.domain.repository.AvisoRepository;
import com.crenx.data.domain.vo.AvisoToVOModelMap;
import com.crenx.data.domain.vo.AvisoVO;
import com.crenx.data.domain.vo.FiltrosAvisosVO;
import java.lang.reflect.Type;

@Service
public class AvisoServices {
	@Autowired
	private AvisoRepository avisoRepository;
	
	public Collection<AvisoVO> filtrarAvisos(FiltrosAvisosVO filtros){
		Collection<Aviso> avisos = null;
		
		String asunto = StringUtils.isEmpty(filtros.getAsunto())?"%":filtros.getAsunto()+"%";
		List<String> categorias = Lists.newArrayList(filtros.getCategorias());
		
		if (filtros.getCategorias().size()>0) {
			avisos = Lists.newArrayList(this.avisoRepository.findByIdTipoAviso_IdCatalogoInAndAsunto(filtros.getCategorias(), filtros.getAsunto()));
		}else{
			avisos = Lists.newArrayList(this.avisoRepository.findByAsuntoIsLike(asunto));
		}
		
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new AvisoToVOModelMap());
		Type listType = new TypeToken<List<AvisoVO>>() {}.getType();
		List<AvisoVO> listaAvisos = mapper.map(avisos, listType);
		return listaAvisos;
	}

}
