package com.crenx.data.domain.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.crenx.data.domain.entity.ObjetoSeguridadRelaciones;

public interface ObjetoSeguridadRelacionesRepository extends CrudRepository<ObjetoSeguridadRelaciones,String> {

	List<ObjetoSeguridadRelaciones> findByParentIdObjetoSeguridad(String idParent);
	List<ObjetoSeguridadRelaciones> findByParentIdObjetoSeguridadAndChildEstatusActualClaveInterna(String idParent, String claveInterna);
}
