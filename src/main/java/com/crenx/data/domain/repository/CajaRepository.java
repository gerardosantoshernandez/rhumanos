package com.crenx.data.domain.repository;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.crenx.data.domain.entity.Caja;
import com.crenx.data.domain.vo.ResumenEmpresaVO;

public interface CajaRepository extends CrudRepository<Caja,String>{
	Caja findByUnidadIdOrg(String idOrg);
	Caja findTop1ByUnidadIdOrgOrderByFechaCorteDesc(String idOrg);
	List<Caja> findByFechaCorteAfter(Date fechaCorte);
	List<Caja>findByUnidadIdOrgInAndFechaCorteBetweenOrderByUnidad(List<String> orgs,Date fechaInicial, Date fechaFinal);

	//Para Resumen Empresarial, todos los saldos
	@Query("select new com.crenx.data.domain.vo.ResumenEmpresaVO (r.nombre as ruta, g.nombre as gerencia, rr.nombre as region, d.nombre as division, COALESCE(sum(c.saldoFinal),0.0) as monto, sum(0.0) as abono, sum(0.0) as comision ) "
			+ "FROM Caja c  "
			+ "JOIN c.unidad r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "JOIN rr.parent d "
			+ "WHERE r.idOrg in (:filtros)  "
			+ "AND c.saldoFinal < :rangoSaldo "
			//+ "GROUP BY r.nombre , g.nombre, rr.nombre, d.nombre "
			+ "GROUP BY d.nombre, rr.nombre, g.nombre, r.nombre "
			+ "ORDER BY d.nombre, rr.nombre, g.nombre, r.nombre")
	List<ResumenEmpresaVO> findByRutasAndFechas(@Param("filtros") List<String> rutas, @Param("rangoSaldo") double rangoSaldo );
			
}
