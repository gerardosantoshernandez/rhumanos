package com.crenx.data.domain.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.crenx.data.domain.entity.Parametros;

public interface ParametrosRepository extends CrudRepository<Parametros,String> {

	List<Parametros> findByNombre(String nombre);
}
