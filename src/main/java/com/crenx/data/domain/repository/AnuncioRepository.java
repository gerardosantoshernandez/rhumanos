package com.crenx.data.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Anuncio;

public interface AnuncioRepository extends CrudRepository<Anuncio, String>{

}
