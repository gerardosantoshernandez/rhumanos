package com.crenx.data.domain.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.ReglaCredito;

public interface ReglaCreditoRepository extends CrudRepository<ReglaCredito,String>{

	List<ReglaCredito> findByRolLlaveNegocioIn(List<String> roles);
}
