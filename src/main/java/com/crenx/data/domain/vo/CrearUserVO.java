package com.crenx.data.domain.vo;

import java.util.Date;

import javax.persistence.Column;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Domicilio;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CrearUserVO {

	private String idUsuario;
	private String nombreUsuario;
	private String contrasena;
	private String contrasenaConfirm;
	private String correoElectronico;
	private String nombre;
	private String apellidoMaterno;
	private String apellidoPaterno;
	
	@JsonProperty(required=true)
	private String tipoDocId;
	private String docIdentificacion;
	private String idGenero;
	@JsonProperty(required=true)
	private String fechaNacimiento;
	private String fechaIngreso;
	private String curp;
	private String idCargo;
	private String cveFiscal;
	private boolean activo=true;
	
	private double salarioBase=0;
	private double salarioAsimilado=0;
	private int semanasGracia = 0;	
	private String cveEmpleado;
	private String fechaBaja;
	
	private Domicilio domicilioEmpleado;
	private Domicilio domicilioEmpleadoLaboral;
	private String telefonoCasa;
	private String telefonoCelular;
	private String idEntidadNacimiento;
	
	//Nuevos atributos
	
	private String idEstadoCivil;
	private String idPosicionEmpleado;
	private String idNacionalidad;
	private String referenciaFamiliar;
	private String telefonoReFamiliar;
	
	private String referenciaFamiliarDos;
	private String telefonoReFamiliarDos;
	private String referenciaFamiliarTres;
	private String telefonoReFamiliarTres;
	private String referenciaFamiliarCua;
	private String telefonoReFamiliarCua;
	
	//Datos de informacion laboral
	private String empledoEmpresa;
	private String departamentoEmpresa;
	private String emailEmpre;
	private String idPosicionEmpresa;
	private String idSectorEmpresa;
	private String telefonoEmpresa;
	
	//-------------------------------------
	private String idMesInicio;
	private String idMesFinal;
	private String idAnioInicio;
	private String idAnioFinal;
	private String descripcionEmpresa;
	
	//---------------------------------------------
	private String nombreEmpresaAnt;
	private String departamentoEmpresaAnt;
	private String emailEmpresaAnt;
	private String telefonoEmpresaAnt;
	private String descripcionEmpresaAnt;
	private String idPosicionEmpresaAnt;
	private String idSectorEmpresaAnt;
	private String idMesInicioAnt;
	private String idMesFinalAnt;
	private String idAnioInicioAnt;
	private String idAnioFinalAnt;
	
	//-------------------------------------------------
	private String nombreProyecto;
	private String idCategoriaProyecto;
	private String idMesInicioPro;
	private String idMesFinalPro;
	private String idAnioInicioPro;
	private String idAnioFinalPro;
	private String urlProyecto;
	private String descripcionPro;
	
	//-------------------------------------------------
	private String institutoPri;
	private String codigoPostalPri;
	private String municipioPri;
	private String estadoPri;
	private String paisPri;
	private String notaPri;
	private String actividadPri;
	private String idTituloPri;
	private String idInicioPri;
	private String idFinPri;
	
	private String institutoSecu;
	private String codigoPostalSecu;
	private String municipioSecu;
	private String estadoSecu;
	private String paisSecu;
	private String notaSecu;
	private String actividadSecu;
	private String idTituloSecu;
	private String idInicioSecu;
	private String idFinSecu;
	
	private String institutoPre;
	private String codigoPostalPre;
	private String municipioPre;
	private String estadoPre;
	private String paisPre;
	private String notaPre;
	private String actividadPre;
	private String idTituloPre;
	private String idInicioPre;
	private String idFinPre;
	
	private String institutoUni;
	private String codigoPostalUni;
	private String municipioUni;
	private String estadoUni;
	private String paisUni;
	private String notaUni;
	private String actividadUni;
	private String idTituloUni;
	private String idInicioUni;
	private String idFinUni;
	
	private String institutoPos;
	private String codigoPostalPos;
	private String municipioPos;
	private String estadoPos;
	private String paisPos;
	private String notaPos;
	private String actividadPos;
	private String idTituloPos;
	private String idInicioPos;
	private String idFinPos;
	
	private String institutoDoc;
	private String codigoPostalDoc;
	private String municipioDoc;
	private String estadoDoc;
	private String paisDoc;
	private String notaDoc;
	private String actividadDoc;
	private String idTituloDoc;
	private String idInicioDoc;
	private String idFinDoc;
	
	private String nomberCertificacion;
	private String urlCertificacion;
	private String claveCertificacion;
	private String idCursoCertificacion;
	private String idAnioInicioCertificacion;
	private String idAnioFinalCertificacion;
	
	private String nombreCurso;
	private String folioCurso;
	private String urlCurso;
	private String idDocumentoCurso;
	private String idMesInicioCurso;
	private String idMesFinalCurso;
	
	private String tipoPremio;
	private String emisorPremio;
	private String descripcionPremio;
	private String idAnioPremio;
	
	private String nombreIdioma;
	private String descripcionAptitudes;
	private String idOralIdioma;
	private String idEscritoIdioma;
	private String idComprendidoIdioma;

	public String getTelefonoCasa() {
		return telefonoCasa;
	}
	public String getIdEntidadNacimiento() {
		return idEntidadNacimiento;
	}
	public void setIdEntidadNacimiento(String idEntidadNacimiento) {
		this.idEntidadNacimiento = idEntidadNacimiento;
	}
	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}
	public String getTelefonoCelular() {
		return telefonoCelular;
	}
	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}
	public Domicilio getDomicilioEmpleadoLaboral() {
		return domicilioEmpleadoLaboral;
	}
	public void setDomicilioEmpleadoLaboral(Domicilio domicilioEmpleadoLaboral) {
		this.domicilioEmpleadoLaboral = domicilioEmpleadoLaboral;
	}
	public Domicilio getDomicilioEmpleado() {
		return domicilioEmpleado;
	}
	public void setDomicilioEmpleado(Domicilio domicilioEmpleado) {
		this.domicilioEmpleado = domicilioEmpleado;
	}
	public String getCveFiscal() {
		return cveFiscal;
	}
	public void setCveFiscal(String cveFiscal) {
		this.cveFiscal = cveFiscal;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getContrasenaConfirm() {
		return contrasenaConfirm;
	}
	public void setContrasenaConfirm(String contrasenaConfirm) {
		this.contrasenaConfirm = contrasenaConfirm;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getTipoDocId() {
		return tipoDocId;
	}
	public void setTipoDocId(String tipoDocId) {
		this.tipoDocId = tipoDocId;
	}
	public String getDocIdentificacion() {
		return docIdentificacion;
	}
	public void setDocIdentificacion(String docIdentificacion) {
		this.docIdentificacion = docIdentificacion;
	}
	public String getIdGenero() {
		return idGenero;
	}
	public void setIdGenero(String idGenero) {
		this.idGenero = idGenero;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getIdCargo() {
		return idCargo;
	}
	public void setIdCargo(String idCargo) {
		this.idCargo = idCargo;
	}
	public String getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	public double getSalarioBase() {
		return salarioBase;
	}
	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}
	public double getSalarioAsimilado() {
		return salarioAsimilado;
	}
	public void setSalarioAsimilado(double salarioAsimilado) {
		this.salarioAsimilado = salarioAsimilado;
	}
	public int getSemanasGracia() {
		return semanasGracia;
	}
	public void setSemanasGracia(int semanasGracia) {
		this.semanasGracia = semanasGracia;
	}
	public String getCveEmpleado() {
		return cveEmpleado;
	}
	public void setCveEmpleado(String cveEmpleado) {
		this.cveEmpleado = cveEmpleado;
	}
	public String getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	//Otros
	public String getIdEstadoCivil() {
		return idEstadoCivil;
	}
	public void setIdEstadoCivil(String idEstadoCivil) {
		this.idEstadoCivil = idEstadoCivil;
	}
	public String getIdPosicionEmpleado() {
		return idPosicionEmpleado;
	}
	public void setIdPosicionEmpleado(String idPosicionEmpleado) {
		this.idPosicionEmpleado = idPosicionEmpleado;
	}
	public String getIdNacionalidad() {
		return idNacionalidad;
	}
	public void setIdNacionalidad(String idNacionalidad) {
		this.idNacionalidad = idNacionalidad;
	}
	public String getReferenciaFamiliar() {
		return referenciaFamiliar;
	}
	public void setReferenciaFamiliar(String referenciaFamiliar) {
		this.referenciaFamiliar = referenciaFamiliar;
	}
	public String getTelefonoReFamiliar() {
		return telefonoReFamiliar;
	}
	public void setTelefonoReFamiliar(String telefonoReFamiliar) {
		this.telefonoReFamiliar = telefonoReFamiliar;
	}
	public String getReferenciaFamiliarDos() {
		return referenciaFamiliarDos;
	}
	public void setReferenciaFamiliarDos(String referenciaFamiliarDos) {
		this.referenciaFamiliarDos = referenciaFamiliarDos;
	}
	public String getTelefonoReFamiliarDos() {
		return telefonoReFamiliarDos;
	}
	public void setTelefonoReFamiliarDos(String telefonoReFamiliarDos) {
		this.telefonoReFamiliarDos = telefonoReFamiliarDos;
	}
	public String getReferenciaFamiliarTres() {
		return referenciaFamiliarTres;
	}
	public void setReferenciaFamiliarTres(String referenciaFamiliarTres) {
		this.referenciaFamiliarTres = referenciaFamiliarTres;
	}
	public String getTelefonoReFamiliarTres() {
		return telefonoReFamiliarTres;
	}
	public void setTelefonoReFamiliarTres(String telefonoReFamiliarTres) {
		this.telefonoReFamiliarTres = telefonoReFamiliarTres;
	}
	public String getReferenciaFamiliarCua() {
		return referenciaFamiliarCua;
	}
	public void setReferenciaFamiliarCua(String referenciaFamiliarCua) {
		this.referenciaFamiliarCua = referenciaFamiliarCua;
	}
	public String getTelefonoReFamiliarCua() {
		return telefonoReFamiliarCua;
	}
	public void setTelefonoReFamiliarCua(String telefonoReFamiliarCua) {
		this.telefonoReFamiliarCua = telefonoReFamiliarCua;
	}
	public String getEmpledoEmpresa() {
		return empledoEmpresa;
	}
	public void setEmpledoEmpresa(String empledoEmpresa) {
		this.empledoEmpresa = empledoEmpresa;
	}
	public String getDepartamentoEmpresa() {
		return departamentoEmpresa;
	}
	public void setDepartamentoEmpresa(String departamentoEmpresa) {
		this.departamentoEmpresa = departamentoEmpresa;
	}
	public String getEmailEmpre() {
		return emailEmpre;
	}
	public void setEmailEmpre(String emailEmpre) {
		this.emailEmpre = emailEmpre;
	}	
	public String getIdPosicionEmpresa() {
		return idPosicionEmpresa;
	}
	public void setIdPosicionEmpresa(String idPosicionEmpresa) {
		this.idPosicionEmpresa = idPosicionEmpresa;
	}
	public String getIdSectorEmpresa() {
		return idSectorEmpresa;
	}
	public void setIdSectorEmpresa(String idSectorEmpresa) {
		this.idSectorEmpresa = idSectorEmpresa;
	}
	public String getTelefonoEmpresa() {
		return telefonoEmpresa;
	}
	public void setTelefonoEmpresa(String telefonoEmpresa) {
		this.telefonoEmpresa = telefonoEmpresa;
	}
	public String getIdMesInicio() {
		return idMesInicio;
	}
	public void setIdMesInicio(String idMesInicio) {
		this.idMesInicio = idMesInicio;
	}
	public String getIdMesFinal() {
		return idMesFinal;
	}
	public void setIdMesFinal(String idMesFinal) {
		this.idMesFinal = idMesFinal;
	}
	public String getIdAnioInicio() {
		return idAnioInicio;
	}
	public void setIdAnioInicio(String idAnioInicio) {
		this.idAnioInicio = idAnioInicio;
	}
	public String getIdAnioFinal() {
		return idAnioFinal;
	}
	public void setIdAnioFinal(String idAnioFinal) {
		this.idAnioFinal = idAnioFinal;
	}
	public String getDescripcionEmpresa() {
		return descripcionEmpresa;
	}
	public void setDescripcionEmpresa(String descripcionEmpresa) {
		this.descripcionEmpresa = descripcionEmpresa;
	}
	public String getNombreEmpresaAnt() {
		return nombreEmpresaAnt;
	}
	public void setNombreEmpresaAnt(String nombreEmpresaAnt) {
		this.nombreEmpresaAnt = nombreEmpresaAnt;
	}
	public String getDepartamentoEmpresaAnt() {
		return departamentoEmpresaAnt;
	}
	public void setDepartamentoEmpresaAnt(String departamentoEmpresaAnt) {
		this.departamentoEmpresaAnt = departamentoEmpresaAnt;
	}
	public String getEmailEmpresaAnt() {
		return emailEmpresaAnt;
	}
	public void setEmailEmpresaAnt(String emailEmpresaAnt) {
		this.emailEmpresaAnt = emailEmpresaAnt;
	}
	public String getTelefonoEmpresaAnt() {
		return telefonoEmpresaAnt;
	}
	public void setTelefonoEmpresaAnt(String telefonoEmpresaAnt) {
		this.telefonoEmpresaAnt = telefonoEmpresaAnt;
	}
	public String getDescripcionEmpresaAnt() {
		return descripcionEmpresaAnt;
	}
	public void setDescripcionEmpresaAnt(String descripcionEmpresaAnt) {
		this.descripcionEmpresaAnt = descripcionEmpresaAnt;
	}
	public String getIdPosicionEmpresaAnt() {
		return idPosicionEmpresaAnt;
	}
	public void setIdPosicionEmpresaAnt(String idPosicionEmpresaAnt) {
		this.idPosicionEmpresaAnt = idPosicionEmpresaAnt;
	}
	public String getIdSectorEmpresaAnt() {
		return idSectorEmpresaAnt;
	}
	public void setIdSectorEmpresaAnt(String idSectorEmpresaAnt) {
		this.idSectorEmpresaAnt = idSectorEmpresaAnt;
	}
	public String getIdMesInicioAnt() {
		return idMesInicioAnt;
	}
	public void setIdMesInicioAnt(String idMesInicioAnt) {
		this.idMesInicioAnt = idMesInicioAnt;
	}
	public String getIdMesFinalAnt() {
		return idMesFinalAnt;
	}
	public void setIdMesFinalAnt(String idMesFinalAnt) {
		this.idMesFinalAnt = idMesFinalAnt;
	}
	public String getIdAnioInicioAnt() {
		return idAnioInicioAnt;
	}
	public void setIdAnioInicioAnt(String idAnioInicioAnt) {
		this.idAnioInicioAnt = idAnioInicioAnt;
	}
	public String getIdAnioFinalAnt() {
		return idAnioFinalAnt;
	}
	public void setIdAnioFinalAnt(String idAnioFinalAnt) {
		this.idAnioFinalAnt = idAnioFinalAnt;
	}
	public String getNombreProyecto() {
		return nombreProyecto;
	}
	public void setNombreProyecto(String nombreProyecto) {
		this.nombreProyecto = nombreProyecto;
	}
	public String getIdCategoriaProyecto() {
		return idCategoriaProyecto;
	}
	public void setIdCategoriaProyecto(String idCategoriaProyecto) {
		this.idCategoriaProyecto = idCategoriaProyecto;
	}
	public String getIdMesInicioPro() {
		return idMesInicioPro;
	}
	public void setIdMesInicioPro(String idMesInicioPro) {
		this.idMesInicioPro = idMesInicioPro;
	}
	public String getIdMesFinalPro() {
		return idMesFinalPro;
	}
	public void setIdMesFinalPro(String idMesFinalPro) {
		this.idMesFinalPro = idMesFinalPro;
	}
	public String getIdAnioInicioPro() {
		return idAnioInicioPro;
	}
	public void setIdAnioInicioPro(String idAnioInicioPro) {
		this.idAnioInicioPro = idAnioInicioPro;
	}
	public String getIdAnioFinalPro() {
		return idAnioFinalPro;
	}
	public void setIdAnioFinalPro(String idAnioFinalPro) {
		this.idAnioFinalPro = idAnioFinalPro;
	}
	public String getUrlProyecto() {
		return urlProyecto;
	}
	public void setUrlProyecto(String urlProyecto) {
		this.urlProyecto = urlProyecto;
	}
	public String getDescripcionPro() {
		return descripcionPro;
	}
	public void setDescripcionPro(String descripcionPro) {
		this.descripcionPro = descripcionPro;
	}
	public String getInstitutoPri() {
		return institutoPri;
	}
	public String getCodigoPostalPri() {
		return codigoPostalPri;
	}
	public String getMunicipioPri() {
		return municipioPri;
	}
	public String getEstadoPri() {
		return estadoPri;
	}
	public String getPaisPri() {
		return paisPri;
	}
	public String getNotaPri() {
		return notaPri;
	}
	public String getActividadPri() {
		return actividadPri;
	}
	public String getIdTituloPri() {
		return idTituloPri;
	}
	public String getIdInicioPri() {
		return idInicioPri;
	}
	public String getIdFinPri() {
		return idFinPri;
	}
	public String getInstitutoSecu() {
		return institutoSecu;
	}
	public String getCodigoPostalSecu() {
		return codigoPostalSecu;
	}
	public String getMunicipioSecu() {
		return municipioSecu;
	}
	public String getEstadoSecu() {
		return estadoSecu;
	}
	public String getPaisSecu() {
		return paisSecu;
	}
	public String getNotaSecu() {
		return notaSecu;
	}
	public String getActividadSecu() {
		return actividadSecu;
	}
	public String getIdTituloSecu() {
		return idTituloSecu;
	}
	public String getIdInicioSecu() {
		return idInicioSecu;
	}
	public String getIdFinSecu() {
		return idFinSecu;
	}
	public String getInstitutoPre() {
		return institutoPre;
	}
	public String getCodigoPostalPre() {
		return codigoPostalPre;
	}
	public String getMunicipioPre() {
		return municipioPre;
	}
	public String getEstadoPre() {
		return estadoPre;
	}
	public String getPaisPre() {
		return paisPre;
	}
	public String getNotaPre() {
		return notaPre;
	}
	public String getActividadPre() {
		return actividadPre;
	}
	public String getIdTituloPre() {
		return idTituloPre;
	}
	public String getIdInicioPre() {
		return idInicioPre;
	}
	public String getIdFinPre() {
		return idFinPre;
	}
	public String getInstitutoUni() {
		return institutoUni;
	}
	public String getCodigoPostalUni() {
		return codigoPostalUni;
	}
	public String getMunicipioUni() {
		return municipioUni;
	}
	public String getEstadoUni() {
		return estadoUni;
	}
	public String getPaisUni() {
		return paisUni;
	}
	public String getNotaUni() {
		return notaUni;
	}
	public String getActividadUni() {
		return actividadUni;
	}
	public String getIdTituloUni() {
		return idTituloUni;
	}
	public String getIdInicioUni() {
		return idInicioUni;
	}
	public String getIdFinUni() {
		return idFinUni;
	}
	public String getInstitutoPos() {
		return institutoPos;
	}
	public String getCodigoPostalPos() {
		return codigoPostalPos;
	}
	public String getMunicipioPos() {
		return municipioPos;
	}
	public String getEstadoPos() {
		return estadoPos;
	}
	public String getPaisPos() {
		return paisPos;
	}
	public String getNotaPos() {
		return notaPos;
	}
	public String getActividadPos() {
		return actividadPos;
	}
	public String getIdTituloPos() {
		return idTituloPos;
	}
	public String getIdInicioPos() {
		return idInicioPos;
	}
	public String getIdFinPos() {
		return idFinPos;
	}
	public String getInstitutoDoc() {
		return institutoDoc;
	}
	public String getCodigoPostalDoc() {
		return codigoPostalDoc;
	}
	public String getMunicipioDoc() {
		return municipioDoc;
	}
	public String getEstadoDoc() {
		return estadoDoc;
	}
	public String getPaisDoc() {
		return paisDoc;
	}
	public String getNotaDoc() {
		return notaDoc;
	}
	public String getActividadDoc() {
		return actividadDoc;
	}
	public String getIdTituloDoc() {
		return idTituloDoc;
	}
	public String getIdInicioDoc() {
		return idInicioDoc;
	}
	public String getIdFinDoc() {
		return idFinDoc;
	}
	public void setInstitutoPri(String institutoPri) {
		this.institutoPri = institutoPri;
	}
	public void setCodigoPostalPri(String codigoPostalPri) {
		this.codigoPostalPri = codigoPostalPri;
	}
	public void setMunicipioPri(String municipioPri) {
		this.municipioPri = municipioPri;
	}
	public void setEstadoPri(String estadoPri) {
		this.estadoPri = estadoPri;
	}
	public void setPaisPri(String paisPri) {
		this.paisPri = paisPri;
	}
	public void setNotaPri(String notaPri) {
		this.notaPri = notaPri;
	}
	public void setActividadPri(String actividadPri) {
		this.actividadPri = actividadPri;
	}
	public void setIdTituloPri(String idTituloPri) {
		this.idTituloPri = idTituloPri;
	}
	public void setIdInicioPri(String idInicioPri) {
		this.idInicioPri = idInicioPri;
	}
	public void setIdFinPri(String idFinPri) {
		this.idFinPri = idFinPri;
	}
	public void setInstitutoSecu(String institutoSecu) {
		this.institutoSecu = institutoSecu;
	}
	public void setCodigoPostalSecu(String codigoPostalSecu) {
		this.codigoPostalSecu = codigoPostalSecu;
	}
	public void setMunicipioSecu(String municipioSecu) {
		this.municipioSecu = municipioSecu;
	}
	public void setEstadoSecu(String estadoSecu) {
		this.estadoSecu = estadoSecu;
	}
	public void setPaisSecu(String paisSecu) {
		this.paisSecu = paisSecu;
	}
	public void setNotaSecu(String notaSecu) {
		this.notaSecu = notaSecu;
	}
	public void setActividadSecu(String actividadSecu) {
		this.actividadSecu = actividadSecu;
	}
	public void setIdTituloSecu(String idTituloSecu) {
		this.idTituloSecu = idTituloSecu;
	}
	public void setIdInicioSecu(String idInicioSecu) {
		this.idInicioSecu = idInicioSecu;
	}
	public void setIdFinSecu(String idFinSecu) {
		this.idFinSecu = idFinSecu;
	}
	public void setInstitutoPre(String institutoPre) {
		this.institutoPre = institutoPre;
	}
	public void setCodigoPostalPre(String codigoPostalPre) {
		this.codigoPostalPre = codigoPostalPre;
	}
	public void setMunicipioPre(String municipioPre) {
		this.municipioPre = municipioPre;
	}
	public void setEstadoPre(String estadoPre) {
		this.estadoPre = estadoPre;
	}
	public void setPaisPre(String paisPre) {
		this.paisPre = paisPre;
	}
	public void setNotaPre(String notaPre) {
		this.notaPre = notaPre;
	}
	public void setActividadPre(String actividadPre) {
		this.actividadPre = actividadPre;
	}
	public void setIdTituloPre(String idTituloPre) {
		this.idTituloPre = idTituloPre;
	}
	public void setIdInicioPre(String idInicioPre) {
		this.idInicioPre = idInicioPre;
	}
	public void setIdFinPre(String idFinPre) {
		this.idFinPre = idFinPre;
	}
	public void setInstitutoUni(String institutoUni) {
		this.institutoUni = institutoUni;
	}
	public void setCodigoPostalUni(String codigoPostalUni) {
		this.codigoPostalUni = codigoPostalUni;
	}
	public void setMunicipioUni(String municipioUni) {
		this.municipioUni = municipioUni;
	}
	public void setEstadoUni(String estadoUni) {
		this.estadoUni = estadoUni;
	}
	public void setPaisUni(String paisUni) {
		this.paisUni = paisUni;
	}
	public void setNotaUni(String notaUni) {
		this.notaUni = notaUni;
	}
	public void setActividadUni(String actividadUni) {
		this.actividadUni = actividadUni;
	}
	public void setIdTituloUni(String idTituloUni) {
		this.idTituloUni = idTituloUni;
	}
	public void setIdInicioUni(String idInicioUni) {
		this.idInicioUni = idInicioUni;
	}
	public void setIdFinUni(String idFinUni) {
		this.idFinUni = idFinUni;
	}
	public void setInstitutoPos(String institutoPos) {
		this.institutoPos = institutoPos;
	}
	public void setCodigoPostalPos(String codigoPostalPos) {
		this.codigoPostalPos = codigoPostalPos;
	}
	public void setMunicipioPos(String municipioPos) {
		this.municipioPos = municipioPos;
	}
	public void setEstadoPos(String estadoPos) {
		this.estadoPos = estadoPos;
	}
	public void setPaisPos(String paisPos) {
		this.paisPos = paisPos;
	}
	public void setNotaPos(String notaPos) {
		this.notaPos = notaPos;
	}
	public void setActividadPos(String actividadPos) {
		this.actividadPos = actividadPos;
	}
	public void setIdTituloPos(String idTituloPos) {
		this.idTituloPos = idTituloPos;
	}
	public void setIdInicioPos(String idInicioPos) {
		this.idInicioPos = idInicioPos;
	}
	public void setIdFinPos(String idFinPos) {
		this.idFinPos = idFinPos;
	}
	public void setInstitutoDoc(String institutoDoc) {
		this.institutoDoc = institutoDoc;
	}
	public void setCodigoPostalDoc(String codigoPostalDoc) {
		this.codigoPostalDoc = codigoPostalDoc;
	}
	public void setMunicipioDoc(String municipioDoc) {
		this.municipioDoc = municipioDoc;
	}
	public void setEstadoDoc(String estadoDoc) {
		this.estadoDoc = estadoDoc;
	}
	public void setPaisDoc(String paisDoc) {
		this.paisDoc = paisDoc;
	}
	public void setNotaDoc(String notaDoc) {
		this.notaDoc = notaDoc;
	}
	public void setActividadDoc(String actividadDoc) {
		this.actividadDoc = actividadDoc;
	}
	public void setIdTituloDoc(String idTituloDoc) {
		this.idTituloDoc = idTituloDoc;
	}
	public void setIdInicioDoc(String idInicioDoc) {
		this.idInicioDoc = idInicioDoc;
	}
	public void setIdFinDoc(String idFinDoc) {
		this.idFinDoc = idFinDoc;
	}
	public String getNomberCertificacion() {
		return nomberCertificacion;
	}
	public String getUrlCertificacion() {
		return urlCertificacion;
	}
	public String getClaveCertificacion() {
		return claveCertificacion;
	}
	public String getIdCursoCertificacion() {
		return idCursoCertificacion;
	}
	public String getIdAnioInicioCertificacion() {
		return idAnioInicioCertificacion;
	}
	public String getIdAnioFinalCertificacion() {
		return idAnioFinalCertificacion;
	}
	public void setNomberCertificacion(String nomberCertificacion) {
		this.nomberCertificacion = nomberCertificacion;
	}
	public void setUrlCertificacion(String urlCertificacion) {
		this.urlCertificacion = urlCertificacion;
	}
	public void setClaveCertificacion(String claveCertificacion) {
		this.claveCertificacion = claveCertificacion;
	}
	public void setIdCursoCertificacion(String idCursoCertificacion) {
		this.idCursoCertificacion = idCursoCertificacion;
	}
	public void setIdAnioInicioCertificacion(String idAnioInicioCertificacion) {
		this.idAnioInicioCertificacion = idAnioInicioCertificacion;
	}
	public void setIdAnioFinalCertificacion(String idAnioFinalCertificacion) {
		this.idAnioFinalCertificacion = idAnioFinalCertificacion;
	}
	public String getNombreCurso() {
		return nombreCurso;
	}
	public String getFolioCurso() {
		return folioCurso;
	}
	public String getUrlCurso() {
		return urlCurso;
	}
	public String getIdDocumentoCurso() {
		return idDocumentoCurso;
	}
	public String getIdMesInicioCurso() {
		return idMesInicioCurso;
	}
	public String getIdMesFinalCurso() {
		return idMesFinalCurso;
	}
	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}
	public void setFolioCurso(String folioCurso) {
		this.folioCurso = folioCurso;
	}
	public void setUrlCurso(String urlCurso) {
		this.urlCurso = urlCurso;
	}
	public void setIdDocumentoCurso(String idDocumentoCurso) {
		this.idDocumentoCurso = idDocumentoCurso;
	}
	public void setIdMesInicioCurso(String idMesInicioCurso) {
		this.idMesInicioCurso = idMesInicioCurso;
	}
	public void setIdMesFinalCurso(String idMesFinalCurso) {
		this.idMesFinalCurso = idMesFinalCurso;
	}
	public String getTipoPremio() {
		return tipoPremio;
	}
	public String getEmisorPremio() {
		return emisorPremio;
	}
	public String getDescripcionPremio() {
		return descripcionPremio;
	}
	public String getIdAnioPremio() {
		return idAnioPremio;
	}
	public void setTipoPremio(String tipoPremio) {
		this.tipoPremio = tipoPremio;
	}
	public void setEmisorPremio(String emisorPremio) {
		this.emisorPremio = emisorPremio;
	}
	public void setDescripcionPremio(String descripcionPremio) {
		this.descripcionPremio = descripcionPremio;
	}
	public void setIdAnioPremio(String idAnioPremio) {
		this.idAnioPremio = idAnioPremio;
	}
	public String getNombreIdioma() {
		return nombreIdioma;
	}
	public String getDescripcionAptitudes() {
		return descripcionAptitudes;
	}
	public String getIdOralIdioma() {
		return idOralIdioma;
	}
	public String getIdEscritoIdioma() {
		return idEscritoIdioma;
	}
	public String getIdComprendidoIdioma() {
		return idComprendidoIdioma;
	}
	public void setNombreIdioma(String nombreIdioma) {
		this.nombreIdioma = nombreIdioma;
	}
	public void setDescripcionAptitudes(String descripcionAptitudes) {
		this.descripcionAptitudes = descripcionAptitudes;
	}
	public void setIdOralIdioma(String idOralIdioma) {
		this.idOralIdioma = idOralIdioma;
	}
	public void setIdEscritoIdioma(String idEscritoIdioma) {
		this.idEscritoIdioma = idEscritoIdioma;
	}
	public void setIdComprendidoIdioma(String idComprendidoIdioma) {
		this.idComprendidoIdioma = idComprendidoIdioma;
	}
	
}
