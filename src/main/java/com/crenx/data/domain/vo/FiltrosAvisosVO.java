package com.crenx.data.domain.vo;

import java.util.List;

public class FiltrosAvisosVO {
	private String asunto;
	private List<String> categorias;
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public List<String> getCategorias() {
		return categorias;
	}
	public void setCategorias(List<String> categorias) {
		this.categorias = categorias;
	}
	
}
