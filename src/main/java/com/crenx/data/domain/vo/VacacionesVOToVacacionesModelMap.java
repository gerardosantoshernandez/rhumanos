package com.crenx.data.domain.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;
import org.modelmapper.AbstractConverter;

import com.crenx.data.domain.entity.Vacaciones;

public class VacacionesVOToVacacionesModelMap extends PropertyMap<VacacionesVO, Vacaciones>{

	@Override
    protected void configure() {
		using(stringToDate).map(source.getFechaSolicitada()).setFechaSolicitada(null);

    }
   Converter<String, Date> stringToDate = new AbstractConverter<String, Date>() {
	   protected Date convert(String source) {
		   if (source==null)
			   return null;
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			try {
				return df.parse(source);
			} catch (ParseException e) {
				System.out.println(e.getMessage());
				return null;
			}
	   }
	 };
}
