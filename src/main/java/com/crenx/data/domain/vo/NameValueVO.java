package com.crenx.data.domain.vo;

public class NameValueVO {

	String id;
	String name;
	public NameValueVO(){}
	
	public NameValueVO(String id, String nombre)
	{
		this.id = id;
		this.name = nombre;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
