package com.crenx.data.domain.vo;

public class PerfilEmpleadoVO {
	
	private String idEmpleado;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String fechaNacimiento;
	private String genero;
	private String curp;
	private String rfc;
	private String tipoDocId;
	private String idTipoDocId;
	private String correoElectonico;
	private String telefonoCasa;
	private String telefonoCelular;
	public String getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getTipoDocId() {
		return tipoDocId;
	}
	public void setTipoDocId(String tipoDocId) {
		this.tipoDocId = tipoDocId;
	}
	public String getIdTipoDocId() {
		return idTipoDocId;
	}
	public void setIdTipoDocId(String idTipoDocId) {
		this.idTipoDocId = idTipoDocId;
	}
	public String getCorreoElectonico() {
		return correoElectonico;
	}
	public void setCorreoElectonico(String correoElectonico) {
		this.correoElectonico = correoElectonico;
	}
	public String getTelefonoCasa() {
		return telefonoCasa;
	}
	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}
	public String getTelefonoCelular() {
		return telefonoCelular;
	}
	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}
}
