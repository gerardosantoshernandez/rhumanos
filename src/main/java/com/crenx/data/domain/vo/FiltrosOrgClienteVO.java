package com.crenx.data.domain.vo;

import java.util.List;

public class FiltrosOrgClienteVO {

	String tipoNodo;
	List<String> filtros;
	public String getTipoNodo() {
		return tipoNodo;
	}
	public void setTipoNodo(String tipoNodo) {
		this.tipoNodo = tipoNodo;
	}
	public List<String> getFiltros() {
		return filtros;
	}
	public void setFiltros(List<String> filtros) {
		this.filtros = filtros;
	}
	
	
}
