package com.crenx.data.domain.vo;

import java.util.Date;

public class DateRangeVO {

	private Date startDate;
	private Date endDate;
	
	private String sDesde;
	private String sHasta;
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getsDesde() {
		return sDesde;
	}
	public void setsDesde(String sDesde) {
		this.sDesde = sDesde;
	}
	public String getsHasta() {
		return sHasta;
	}
	public void setsHasta(String sHasta) {
		this.sHasta = sHasta;
	}
}
