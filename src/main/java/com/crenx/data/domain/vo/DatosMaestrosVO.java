package com.crenx.data.domain.vo;

import java.util.List;

import com.crenx.data.domain.entity.Parametros;
import com.crenx.data.domain.entity.Producto;

public class DatosMaestrosVO {
private List<CatalogoVO> catalogos;
private List<Parametros> parametros;
private List<Producto> productos;

public List<Producto> getProductos() {
	return productos;
}
public void setProductos(List<Producto> productos) {
	this.productos = productos;
}
public List<CatalogoVO> getCatalogos() {
	return catalogos;
}
public void setCatalogos(List<CatalogoVO> catalogos) {
	this.catalogos = catalogos;
}
public List<Parametros> getParametros() {
	return parametros;
}
public void setParametros(List<Parametros> parametros) {
	this.parametros = parametros;
}


}
