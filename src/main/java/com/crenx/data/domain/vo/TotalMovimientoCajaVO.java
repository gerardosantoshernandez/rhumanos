package com.crenx.data.domain.vo;

/**
 * Objeto de trasferencia que contiene la información del total de cargo y abono
 * de los movimientos caja agrupados por ruta, movmiento y operación
 * 
 * @author JCHR
 *
 */
public class TotalMovimientoCajaVO {

	
	 /** 
	 * @param idRuta
	 * @param ruta
	 * @param idTipoMovimiento
	 * @param tipoMovimiento
	 * @param idTipoOperación
	 * @param tipoOperación
	 * @param cargo
	 * @param abono
	 */
	public TotalMovimientoCajaVO(String idRuta, String ruta, String idTipoMovimiento, String tipoMovimiento,
			String idTipoOperación, String tipoOperación, double cargo, double abono) {
		super();
		this.idRuta = idRuta;
		this.ruta = ruta;
		this.idTipoMovimiento = idTipoMovimiento;
		this.tipoMovimiento = tipoMovimiento;
		this.idTipoOperación = idTipoOperación;
		this.tipoOperación = tipoOperación;
		this.cargo = cargo;
		this.abono = abono;
	}

	/**
	 * Id de la ruta
	 */
	private String idRuta;
	/**
	 * Nombre de la ruta
	 */
	private String ruta;
	/**
	 * Id del tipo de movimiento
	 */
	private String idTipoMovimiento;
	/**
	 * Nombre del tipo de movimiento
	 */
	private String tipoMovimiento;
	/**
	 * Id del tipo de Operación
	 */
	private String idTipoOperación;
	/**
	 * Nombre del tipo de operación
	 */
	private String tipoOperación;
	/**
	 * Total del cargo de los movimientos de caja
	 */
	private double cargo;
	/**
	 * Total de abono de los movimientos de caja
	 */
	private double abono;

	/**
	 * @return the idRuta
	 */
	public String getIdRuta() {
		return idRuta;
	}

	/**
	 * @param idRuta
	 *            the idRuta to set
	 */
	public void setIdRuta(String idRuta) {
		this.idRuta = idRuta;
	}

	/**
	 * @return the ruta
	 */
	public String getRuta() {
		return ruta;
	}

	/**
	 * @param ruta
	 *            the ruta to set
	 */
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	/**
	 * @return the idTipoMovimiento
	 */
	public String getIdTipoMovimiento() {
		return idTipoMovimiento;
	}

	/**
	 * @param idTipoMovimiento
	 *            the idTipoMovimiento to set
	 */
	public void setIdTipoMovimiento(String idTipoMovimiento) {
		this.idTipoMovimiento = idTipoMovimiento;
	}

	/**
	 * @return the tipoMovimiento
	 */
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	/**
	 * @param tipoMovimiento
	 *            the tipoMovimiento to set
	 */
	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	/**
	 * @return the idTipoOperación
	 */
	public String getIdTipoOperación() {
		return idTipoOperación;
	}

	/**
	 * @param idTipoOperación
	 *            the idTipoOperación to set
	 */
	public void setIdTipoOperación(String idTipoOperación) {
		this.idTipoOperación = idTipoOperación;
	}

	/**
	 * @return the tipoOperación
	 */
	public String getTipoOperación() {
		return tipoOperación;
	}

	/**
	 * @param tipoOperación
	 *            the tipoOperación to set
	 */
	public void setTipoOperación(String tipoOperación) {
		this.tipoOperación = tipoOperación;
	}

	/**
	 * @return the cargo
	 */
	public double getCargo() {
		return cargo;
	}

	/**
	 * @param cargo
	 *            the cargo to set
	 */
	public void setCargo(double cargo) {
		this.cargo = cargo;
	}

	/**
	 * @return the abono
	 */
	public double getAbono() {
		return abono;
	}

	/**
	 * @param abono
	 *            the abono to set
	 */
	public void setAbono(double abono) {
		this.abono = abono;
	}

}
