package com.crenx.data.domain.vo;

import java.util.Collection;

public class FiltroPerfilVO {

	private String correo;
	private String nombre;
	private String apellido;
	private Collection<String> roles;
	private Collection<String> estatus;
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Collection<String> getRoles() {
		return roles;
	}
	public void setRoles(Collection<String> roles) {
		this.roles = roles;
	}
	public Collection<String> getEstatus() {
		return estatus;
	}
	public void setEstatus(Collection<String> estatus) {
		this.estatus = estatus;
	}
	
	
}
