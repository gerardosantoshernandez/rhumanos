package com.crenx.data.domain.vo;

import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.ReglaCredito;

public class ReglaCreditoToVOModelMapper extends PropertyMap<ReglaCredito, ReglaCreditoVO>{
	   @Override
	    protected void configure() {
		   map().setIdRol(source.getRol().getIdObjetoSeguridad());
		   map().setNombreRol(source.getRol().getNombre());
	    }

}
