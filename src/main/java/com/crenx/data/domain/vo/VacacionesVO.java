package com.crenx.data.domain.vo;

public class VacacionesVO {
	private String idVacaciones;
	private String idEmpleado;
	private String empleado;
	private String asunto;
	private String idAsunto;
	private String dias;
	private String descripcion;
	private String fechaSolicitada;
	public String getIdVacaciones() {
		return idVacaciones;
	}
	public void setIdVacaciones(String idVacaciones) {
		this.idVacaciones = idVacaciones;
	}
	public String getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getDias() {
		return dias;
	}
	public void setDias(String dias) {
		this.dias = dias;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getFechaSolicitada() {
		return fechaSolicitada;
	}
	public void setFechaSolicitada(String fechaSolicitada) {
		this.fechaSolicitada = fechaSolicitada;
	}
	public String getIdAsunto() {
		return idAsunto;
	}
	public void setIdAsunto(String idAsunto) {
		this.idAsunto = idAsunto;
	}
	public String getEmpleado() {
		return empleado;
	}
	public void setEmpleado(String empleado) {
		this.empleado = empleado;
	}
	
}
