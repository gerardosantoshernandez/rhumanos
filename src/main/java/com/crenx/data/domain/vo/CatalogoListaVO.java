package com.crenx.data.domain.vo;

import javax.persistence.Column;

public class CatalogoListaVO {

	private String idCatalogo;
	private String nombre;
	public String getIdCatalogo() {
		return idCatalogo;
	}
	public void setIdCatalogo(String idCatalogo) {
		this.idCatalogo = idCatalogo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	} 
	
	
	
}
