package com.crenx.data.domain.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Empleado;
public class EmpleadoToCrearUsrVOMapper	 extends PropertyMap<Empleado,CrearUserVO>{
	   @Override
	    protected void configure() {
		   map().setIdCargo(source.getIdCargoEmpleado());
		   map().setIdGenero(source.getIdGenero());
		   map().setTipoDocId(source.getIdTipoDocId());
		   map().setIdEntidadNacimiento(source.getIdEntidadNacimiento());
		   map().setIdEstadoCivil(source.getIdEstadoCivil());
		   map().setIdPosicionEmpleado(source.getIdPosicionEmpleado());
		   map().setIdNacionalidad(source.getIdNacionalidad());
		   map().setIdPosicionEmpresa(source.getIdPosicionEmpresa());
		   
		   map().setIdMesInicio(source.getIdMesInicio());
		   map().setIdMesFinal(source.getIdMesFinal());
		   map().setIdAnioInicio(source.getIdAnioInicio());
		   map().setIdAnioFinal(source.getIdAnioFinal());
		   
		   //-------------------------------------
		   map().setIdPosicionEmpresaAnt(source.getIdPosicionEmpresaAnt());
		   map().setIdSectorEmpresaAnt(source.getIdSectorEmpresaAnt());
		   map().setIdMesInicioAnt(source.getIdMesInicioAnt());
		   map().setIdMesFinalAnt(source.getIdMesFinalAnt());
		   map().setIdAnioInicioAnt(source.getIdAnioInicioAnt());
		   map().setIdAnioFinalAnt(source.getIdAnioFinalAnt());
		   
		 //-------------------------------------
		   map().setIdCategoriaProyecto(source.getIdCategoriaProyecto());
		   map().setIdMesInicioPro(source.getIdMesInicioPro());
		   map().setIdMesFinalPro(source.getIdMesFinalPro());
		   map().setIdAnioInicioPro(source.getIdAnioInicioPro());
		   map().setIdAnioFinalPro(source.getIdAnioFinalPro());
		   
		 //-------------------------------------
		   /*map().setIdTituloPri(source.getIdTituloPri());
		   map().setIdInicioPri(source.getIdInicioPri());
		   map().setIdFinPri(source.getIdFinPri());
		 
		 //-------------------------------------
		   map().setIdTituloSecu(source.getIdTituloSecu());
		   map().setIdInicioSecu(source.getIdInicioSecu());
		   map().setIdFinSecu(source.getIdFinSecu());
		 //-------------------------------------
		   map().setIdTituloPre(source.getIdTituloPre());
		   map().setIdInicioPre(source.getIdInicioPre());
		   map().setIdFinPre(source.getIdFinPre());
		 //-------------------------------------
		   map().setIdTituloUni(source.getIdTituloUni());
		   map().setIdInicioUni(source.getIdInicioUni());
		   map().setIdFinUni(source.getIdFinUni());
		 //-------------------------------------
		   map().setIdTituloPos(source.getIdTituloPos());
		   map().setIdInicioPos(source.getIdInicioPos());
		   map().setIdFinPos(source.getIdFinPos());
		 //-------------------------------------
		   map().setIdTituloDoc(source.getIdTituloDoc());
		   map().setIdInicioDoc(source.getIdInicioDoc());
		   map().setIdFinDoc(source.getIdFinDoc());*/
		   
		   
		   using(dateToString).map(source.getFechaNacimiento()).setFechaNacimiento(null);
		   using(dateToString).map(source.getFechaIngreso()).setFechaIngreso(null);
		   using(dateToString).map(source.getFechaBaja()).setFechaBaja(null);

	    }
	   Converter<String, Date> stringToDate = new AbstractConverter<String, Date>() {
		   protected Date convert(String source) {
			   if (source==null)
				   return null;
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				try {
					return df.parse(source);
				} catch (ParseException e) {
					System.out.println(e.getMessage());
					return null;
				}
		   }
		 };

	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };
		 
}
