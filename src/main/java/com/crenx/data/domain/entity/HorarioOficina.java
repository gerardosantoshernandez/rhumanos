package com.crenx.data.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class HorarioOficina {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_horarioOficina", unique = true)
	private String idHorarioOficina;
	
	@Column(name = "id_calendario",length = 10)
	private String idCalendario;
	
	@Column(name = "id_horario",length = 10)
	private String idHorario;

	public String getIdHorarioOficina() {
		return idHorarioOficina;
	}

	public void setIdHorarioOficina(String idHorarioOficina) {
		this.idHorarioOficina = idHorarioOficina;
	}

	public String getIdCalendario() {
		return idCalendario;
	}

	public void setIdCalendario(String idCalendario) {
		this.idCalendario = idCalendario;
	}

	public String getIdHorario() {
		return idHorario;
	}

	public void setIdHorario(String idHorario) {
		this.idHorario = idHorario;
	}

}
