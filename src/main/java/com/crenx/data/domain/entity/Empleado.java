package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Empleado {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "id_empleado", unique = true)
	private String idEmpleado;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo_documento_identidad", nullable = false, referencedColumnName = "id_catalogo")
	private Catalogo tipoDocId;

	// unique=true
	@Column(name = "cve_fiscal", length = 100)
	private String cveFiscal;

	@Column(name = "doc_identificacion", length = 50)
	private String docIdentificacion;

	@Column(name = "fecha_ingreso", nullable = false)
	private Date fechaIngreso;

	@Column(name = "fecha_baja", nullable = true)
	private Date fechaBaja;

	@Column(name = "nombre", length = 50)
	private String nombre;

	@Column(name = "apellido_paterno", length = 50)
	private String apellidoPaterno;

	@Column(name = "apellido_materno", length = 50)
	private String apellidoMaterno;

	@Column(name = "correo_electronico", length = 100)
	private String correoElectronico;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_genero", nullable = false, referencedColumnName = "id_catalogo")
	private Catalogo genero;

	@Column(name = "fecha_nacimiento", nullable = false)
	private Date fechaNacimiento;

	// unique=true
	@Column(name = "curp", length = 20)
	private String curp;

	@Column(name = "fecha_creacion")
	private Date creacion;

	@Column(name = "nombre_usuario", length = 50, unique = true)
	private String nombreUsuario;

	@OneToOne
	@JoinColumn(name = "id_estatus", nullable = false, referencedColumnName = "id_catalogo")
	private Catalogo estatusActual;

	@Column(name = "salarioBase")
	private double salarioBase = 0;

	@Column(name = "salarioAsimilado")
	private double salarioAsimilado = 0;

	@Column(name = "semana_gracia")
	private int semanasGracia = 0;

	@Column(name = "clave_empleado", length = 10, unique = true)
	private String cveEmpleado;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_cargo", nullable = false, referencedColumnName = "id_catalogo")
	private Catalogo cargoEmpleado;

	// Propiedades nuevas
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_domicilio", nullable = true, referencedColumnName = "id_domicilio")
	private Domicilio domicilioEmpleado;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_domicilio_lab", nullable = true, referencedColumnName = "id_domicilio")
	private Domicilio domicilioEmpleadoLaboral;

	@Column(name = "tel_casa", length = 20)
	private String telefonoCasa;

	@Column(name = "tel_celular", length = 20)
	private String telefonoCelular;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_entidad_nacimiento", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo entidadNacimientoEmpl;

	// Nuevos atributos Datos Generales

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_estado_civil", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo estadoCivil;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_posicion", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo posicionEmpleado;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_nacionalidad", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo nacionalidad;

	@Column(name = "id_referencia_familiar", length = 50)
	private String referenciaFamiliar;

	@Column(name = "tel_referencia_familiar", length = 20)
	private String telefonoReFamiliar;

	@Column(name = "id_referencia_familiar_dos", length = 50)
	private String referenciaFamiliarDos;

	@Column(name = "tel_referencia_familiar_dos", length = 20)
	private String telefonoReFamiliarDos;

	@Column(name = "id_referencia_familiar_tres", length = 50)
	private String referenciaFamiliarTres;

	@Column(name = "tel_referencia_familiar_tres", length = 20)
	private String telefonoReFamiliarTres;

	@Column(name = "id_referencia_familiar_cua", length = 50)
	private String referenciaFamiliarCua;

	@Column(name = "tel_referencia_familiar_cua", length = 20)
	private String telefonoReFamiliarCua;

	// Informacion Laboral Empleo
	@Column(name = "empleo_empresa", length = 50)
	private String empledoEmpresa;

	@Column(name = "departamento_empresa", length = 50)
	private String departamentoEmpresa;

	@Column(name = "email_empresa", length = 50)
	private String emailEmpre;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_posicion_empresa", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo posicionEmpresa;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_sector_empresa", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo sectorEmpresa;

	@Column(name = "telefono_empresa_empl", length = 50)
	private String telefonoEmpresa;

	// ------------------------------------------------------------------------------------------------------
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_inicio", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo mesInicio;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_final", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo mesFinal;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_anioInicio", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo anioInicio;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_anioFinal", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo anioFinal;

	// ------------------------------------------------------------------------------------------------------
	// Experiencia Laboral
	@Column(name = "id_nomb_empr", length = 50)
	private String nombreEmpresaAnt;

	@Column(name = "depa_empr_ant", length = 50)
	private String departamentoEmpresaAnt;

	@Column(name = "email_empr_ant", length = 50)
	private String emailEmpresaAnt;

	@Column(name = "telefono_emp_ant", length = 50)
	private String telefonoEmpresaAnt;

	@Column(name = "descripcion_emp_ant", length = 300)
	private String descripcionEmpresaAnt;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_posicion_empresa_ant", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo posicionEmpresaAnt;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_sector_empresa_ant", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo sectorEmpresaAnt;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_inicio_ant", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo mesInicioAnt;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_final_ant", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo mesFinalAnt;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_anioInicio_ant", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo anioInicioAnt;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_anioFinal_ant", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo anioFinalAnt;

	@Column(name = "descripcion_emp", length = 300)
	private String descripcionEmpresa;

	// ------------------------------------------------------------------------------------------------------
	@Column(name = "nombre_proyecto", length = 50)
	private String nombreProyecto;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_categoria_pro", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo categoriaProyecto;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_inicio_pro", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo mesInicioPro;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_final_pro", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo mesFinalPro;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_anioInicio_pro", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo anioInicioPro;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_periodo_anioFinal_pro", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo anioFinalPro;

	@Column(name = "url_pro", length = 200)
	private String urlProyecto;

	@Column(name = "descripcion_pro", length = 300)
	private String descripcionPro;

	// -------------------------------------------------------------------------------------------------------
	@Column(name = "instituto_pri", length = 50)
	private String institutoPri;

	@Column(name = "codigo_pri", length = 50)
	private String codigoPostalPri;

	@Column(name = "municipio_pri", length = 50)
	private String municipioPri;

	@Column(name = "estado_pri", length = 50)
	private String estadoPri;

	@Column(name = "pais_pri", length = 50)
	private String paisPri;

	@Column(name = "nota_pri", length = 50)
	private String notaPri;

	@Column(name = "actividad_prim", length = 50)
	private String actividadPri;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_titulo_pri", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo tituloPri;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_inicio_pri", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo inicioPri;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_fin_pri", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo finPri;

	// -------------------------------------------------------------------------------------------------------
	@Column(name = "instituto_secu", length = 50)
	private String institutoSecu;

	@Column(name = "codigo_secu", length = 50)
	private String codigoPostalSecu;

	@Column(name = "municipio_secu", length = 50)
	private String municipioSecu;

	@Column(name = "estado_secu", length = 50)
	private String estadoSecu;

	@Column(name = "pais_secu", length = 50)
	private String paisSecu;

	@Column(name = "nota_secu", length = 50)
	private String notaSecu;

	@Column(name = "actividad_secu", length = 50)
	private String actividadSecu;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_titulo_secu", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo tituloSecu;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_inicio_secu", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo inicioSecu;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_fin_secu", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo finSecu;

	// -------------------------------------------------------------------------------------------------------
	@Column(name = "instituto_pre", length = 50)
	private String institutoPre;

	@Column(name = "codigo_pre", length = 50)
	private String codigoPostalPre;

	@Column(name = "municipio_pre", length = 50)
	private String municipioPre;

	@Column(name = "estado_pre", length = 50)
	private String estadoPre;

	@Column(name = "pais_pre", length = 50)
	private String paisPre;

	@Column(name = "nota_pre", length = 50)
	private String notaPre;

	@Column(name = "actividad_pre", length = 50)
	private String actividadPre;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_titulo_pre", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo tituloPre;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_inicio_pre", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo inicioPre;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_fin_pre", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo finPre;

	// -------------------------------------------------------------------------------------------------------
	@Column(name = "instituto_uni", length = 50)
	private String institutoUni;

	@Column(name = "codigo_uni", length = 50)
	private String codigoPostalUni;

	@Column(name = "municipio_uni", length = 50)
	private String municipioUni;

	@Column(name = "estado_uni", length = 50)
	private String estadoUni;

	@Column(name = "pais_uni", length = 50)
	private String paisUni;

	@Column(name = "nota_uni", length = 50)
	private String notaUni;

	@Column(name = "actividad_uni", length = 50)
	private String actividadUni;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_titulo_uni", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo tituloUni;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_inicio_uni", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo inicioUni;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_fin_uni", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo finUni;

	// -------------------------------------------------------------------------------------------------------
	@Column(name = "instituto_pos", length = 50)
	private String institutoPos;

	@Column(name = "codigo_pos", length = 50)
	private String codigoPostalPos;

	@Column(name = "municipio_pos", length = 50)
	private String municipioPos;

	@Column(name = "estado_pos", length = 50)
	private String estadoPos;

	@Column(name = "pais_pos", length = 50)
	private String paisPos;

	@Column(name = "nota_pos", length = 50)
	private String notaPos;

	@Column(name = "actividad_pos", length = 50)
	private String actividadPos;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_titulo_pos", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo tituloPos;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_inicio_pos", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo inicioPos;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_fin_pos", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo finPos;

	// -------------------------------------------------------------------------------------------------------
	@Column(name = "instituto_doc", length = 50)
	private String institutoDoc;

	@Column(name = "codigo_doc", length = 50)
	private String codigoPostalDoc;

	@Column(name = "municipio_doc", length = 50)
	private String municipioDoc;

	@Column(name = "estado_doc", length = 50)
	private String estadoDoc;

	@Column(name = "pais_doc", length = 50)
	private String paisDoc;

	@Column(name = "nota_doc", length = 50)
	private String notaDoc;

	@Column(name = "actividad_doc", length = 50)
	private String actividadDoc;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_titulo_doc", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo tituloDoc;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_inicio_doc", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo inicioDoc;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_fin_doc", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo finDoc;

	// -------------------------------------------------------------------------------------------------------
	@Column(name = "nombre_certif", length = 50)
	private String nomberCertificacion;

	@Column(name = "url_certif", length = 50)
	private String urlCertificacion;

	@Column(name = "clave_certif", length = 50)
	private String claveCertificacion;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_curso_certif", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo cursoCertificacion;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_anio_ini_certif", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo anioInicioCertificacion;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_anio_fin_certif", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo anioFinalCertificacion;

	// -------------------------------------------------------------------------------------------------------
	@Column(name = "nombre_curso", length = 50)
	private String nombreCurso;

	@Column(name = "folio_curso", length = 50)
	private String folioCurso;

	@Column(name = "url_curso", length = 50)
	private String urlCurso;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_documento_curso", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo documentoCurso;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_mes_ini_curso", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo mesInicioCurso;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_mes_fin_curso", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo mesFinalCurso;

	// -------------------------------------------------------------------------------------------------------
	@Column(name = "tipo_premio", length = 50)
	private String tipoPremio;

	@Column(name = "emisor_premio", length = 50)
	private String emisorPremio;

	@Column(name = "descripicion_premio", length = 300)
	private String descripcionPremio;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_anio_premio", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo anioPremio;

	// -------------------------------------------------------------------------------------------------------
	@Column(name = "nombre_idioma", length = 50)
	private String nombreIdioma;

	@Column(name = "descripcion_aptitud", length = 300)
	private String descripcionAptitudes;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_oral_idioma", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo oralIdioma;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_escrito_idioma", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo escritoIdioma;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_comprendido_idioma", nullable = true, referencedColumnName = "id_catalogo")
	private Catalogo comprendidoIdioma;

	public String getTelefonoCasa() {
		return telefonoCasa;
	}

	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	public Domicilio getDomicilioEmpleadoLaboral() {
		return domicilioEmpleadoLaboral;
	}

	public void setDomicilioEmpleadoLaboral(Domicilio domicilioEmpleadoLaboral) {
		this.domicilioEmpleadoLaboral = domicilioEmpleadoLaboral;
	}

	public String getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public Catalogo getTipoDocId() {
		return tipoDocId;
	}

	public void setTipoDocId(Catalogo tipoDocId) {
		this.tipoDocId = tipoDocId;
	}

	public String getCveFiscal() {
		return cveFiscal;
	}

	public void setCveFiscal(String cveFiscal) {
		this.cveFiscal = cveFiscal;
	}

	public String getDocIdentificacion() {
		return docIdentificacion;
	}

	public void setDocIdentificacion(String docIdentificacion) {
		this.docIdentificacion = docIdentificacion;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public Catalogo getGenero() {
		return genero;
	}

	public void setGenero(Catalogo genero) {
		this.genero = genero;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public Date getCreacion() {
		return creacion;
	}

	public void setCreacion(Date creacion) {
		this.creacion = creacion;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public Catalogo getCargoEmpleado() {
		return cargoEmpleado;
	}

	public void setCargoEmpleado(Catalogo cargoEmpleado) {
		this.cargoEmpleado = cargoEmpleado;
	}

	public Domicilio getDomicilioEmpleado() {
		return domicilioEmpleado;
	}

	public void setDomicilioEmpleado(Domicilio domicilioEmpleado) {
		this.domicilioEmpleado = domicilioEmpleado;
	}

	public String getIdTipoDocId() {
		String idTipodocId = null;
		if (tipoDocId != null) {
			idTipodocId = tipoDocId.getIdCatalogo();
		}
		return idTipodocId;
	}

	public String getIdGenero() {
		String idGenero = null;
		if (genero != null) {
			idGenero = genero.getIdCatalogo();
		}
		return idGenero;
	}

	public String getIdCargoEmpleado() {
		String idCargoEmpleado = null;
		if (cargoEmpleado != null) {
			idCargoEmpleado = cargoEmpleado.getIdCatalogo();
		}
		return idCargoEmpleado;
	}

	public String getNombreCompleto() {
		return this.nombre + " " + this.getApellidoPaterno() + " " + this.getApellidoMaterno();
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public Catalogo getEstatusActual() {
		return estatusActual;
	}

	public void setEstatusActual(Catalogo estatusActual) {
		this.estatusActual = estatusActual;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public int getSemanasGracia() {
		return semanasGracia;
	}

	public void setSemanasGracia(int semanasGracia) {
		this.semanasGracia = semanasGracia;
	}

	public String getCveEmpleado() {
		return cveEmpleado;
	}

	public void setCveEmpleado(String cveEmpleado) {
		this.cveEmpleado = cveEmpleado;
	}

	public double getSalarioAsimilado() {
		return salarioAsimilado;
	}

	public void setSalarioAsimilado(double salarioAsimilado) {
		this.salarioAsimilado = salarioAsimilado;
	}

	public Catalogo getEntidadNacimientoEmpl() {
		return entidadNacimientoEmpl;
	}

	public void setEntidadNacimientoEmpl(Catalogo entidadNacimientoEmpl) {
		this.entidadNacimientoEmpl = entidadNacimientoEmpl;
	}

	public String getIdEntidadNacimiento() {
		String idEntidadNacimiento = null;
		if (entidadNacimientoEmpl != null) {
			idEntidadNacimiento = entidadNacimientoEmpl.getIdCatalogo();
		}
		return idEntidadNacimiento;
	}

	public String getIdEstadoCivil() {
		String edoCivil = null;
		if (estadoCivil != null) {
			edoCivil = estadoCivil.getIdCatalogo();
		}
		return edoCivil;
	}

	public String getIdPosicionEmpleado() {
		String posicionEmpl = null;
		if (posicionEmpleado != null) {
			posicionEmpl = posicionEmpleado.getIdCatalogo();
		}
		return posicionEmpl;
	}

	public String getIdNacionalidad() {
		String naci = null;
		if (nacionalidad != null) {
			naci = nacionalidad.getIdCatalogo();
		}
		return naci;
	}

	public Catalogo getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(Catalogo estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Catalogo getPosicionEmpleado() {
		return posicionEmpleado;
	}

	public void setPosicionEmpleado(Catalogo posicionEmpleado) {
		this.posicionEmpleado = posicionEmpleado;
	}

	public Catalogo getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(Catalogo nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getReferenciaFamiliar() {
		return referenciaFamiliar;
	}

	public void setReferenciaFamiliar(String referenciaFamiliar) {
		this.referenciaFamiliar = referenciaFamiliar;
	}

	public String getTelefonoReFamiliar() {
		return telefonoReFamiliar;
	}

	public void setTelefonoReFamiliar(String telefonoReFamiliar) {
		this.telefonoReFamiliar = telefonoReFamiliar;
	}

	public String getReferenciaFamiliarDos() {
		return referenciaFamiliarDos;
	}

	public void setReferenciaFamiliarDos(String referenciaFamiliarDos) {
		this.referenciaFamiliarDos = referenciaFamiliarDos;
	}

	public String getTelefonoReFamiliarDos() {
		return telefonoReFamiliarDos;
	}

	public void setTelefonoReFamiliarDos(String telefonoReFamiliarDos) {
		this.telefonoReFamiliarDos = telefonoReFamiliarDos;
	}

	public String getReferenciaFamiliarTres() {
		return referenciaFamiliarTres;
	}

	public void setReferenciaFamiliarTres(String referenciaFamiliarTres) {
		this.referenciaFamiliarTres = referenciaFamiliarTres;
	}

	public String getTelefonoReFamiliarTres() {
		return telefonoReFamiliarTres;
	}

	public void setTelefonoReFamiliarTres(String telefonoReFamiliarTres) {
		this.telefonoReFamiliarTres = telefonoReFamiliarTres;
	}

	public String getReferenciaFamiliarCua() {
		return referenciaFamiliarCua;
	}

	public void setReferenciaFamiliarCua(String referenciaFamiliarCua) {
		this.referenciaFamiliarCua = referenciaFamiliarCua;
	}

	public String getTelefonoReFamiliarCua() {
		return telefonoReFamiliarCua;
	}

	public void setTelefonoReFamiliarCua(String telefonoReFamiliarCua) {
		this.telefonoReFamiliarCua = telefonoReFamiliarCua;
	}

	public String getEmpledoEmpresa() {
		return empledoEmpresa;
	}

	public void setEmpledoEmpresa(String empledoEmpresa) {
		this.empledoEmpresa = empledoEmpresa;
	}

	public String getDepartamentoEmpresa() {
		return departamentoEmpresa;
	}

	public void setDepartamentoEmpresa(String departamentoEmpresa) {
		this.departamentoEmpresa = departamentoEmpresa;
	}

	public String getEmailEmpre() {
		return emailEmpre;
	}

	public void setEmailEmpre(String emailEmpre) {
		this.emailEmpre = emailEmpre;
	}

	public String getIdPosicionEmpresa() {
		String posiEmp = null;
		if (posicionEmpresa != null) {
			posiEmp = posicionEmpresa.getIdCatalogo();
		}
		return posiEmp;
	}

	public Catalogo getPosicionEmpresa() {
		return posicionEmpresa;
	}

	public void setPosicionEmpresa(Catalogo posicionEmpresa) {
		this.posicionEmpresa = posicionEmpresa;
	}

	public String getIdSectorEmpresa() {
		String sectorEmp = null;
		if (sectorEmpresa != null) {
			sectorEmp = sectorEmpresa.getIdCatalogo();
		}
		return sectorEmp;
	}

	public Catalogo getSectorEmpresa() {
		return sectorEmpresa;
	}

	public void setSectorEmpresa(Catalogo sectorEmpresa) {
		this.sectorEmpresa = sectorEmpresa;
	}

	public String getTelefonoEmpresa() {
		return telefonoEmpresa;
	}

	public void setTelefonoEmpresa(String telefonoEmpresa) {
		this.telefonoEmpresa = telefonoEmpresa;
	}

	public String getIdMesInicio() {
		String mesIni = null;
		if (mesInicio != null) {
			mesIni = mesInicio.getIdCatalogo();
		}
		return mesIni;
	}

	public Catalogo getMesInicio() {
		return mesInicio;
	}

	public void setMesInicio(Catalogo mesInicio) {
		this.mesInicio = mesInicio;
	}

	public String getIdMesFinal() {
		String mesFin = null;
		if (mesFinal != null) {
			mesFin = mesFinal.getIdCatalogo();
		}
		return mesFin;
	}

	public Catalogo getMesFinal() {
		return mesFinal;
	}

	public void setMesFinal(Catalogo mesFinal) {
		this.mesFinal = mesFinal;
	}

	public String getIdAnioInicio() {
		String anioIni = null;
		if (anioInicio != null) {
			anioIni = anioInicio.getIdCatalogo();
		}
		return anioIni;
	}

	public Catalogo getAnioInicio() {
		return anioInicio;
	}

	public void setAnioInicio(Catalogo anioInicio) {
		this.anioInicio = anioInicio;
	}

	public String getIdAnioFinal() {
		String anioFin = null;
		if (anioFinal != null) {
			anioFin = anioFinal.getIdCatalogo();
		}
		return anioFin;
	}

	public Catalogo getAnioFinal() {
		return anioFinal;
	}

	public void setAnioFinal(Catalogo anioFinal) {
		this.anioFinal = anioFinal;
	}

	public String getDescripcionEmpresa() {
		return descripcionEmpresa;
	}

	public void setDescripcionEmpresa(String descripcionEmpresa) {
		this.descripcionEmpresa = descripcionEmpresa;
	}

	public String getNombreEmpresaAnt() {
		return nombreEmpresaAnt;
	}

	public void setNombreEmpresaAnt(String nombreEmpresaAnt) {
		this.nombreEmpresaAnt = nombreEmpresaAnt;
	}

	public String getDepartamentoEmpresaAnt() {
		return departamentoEmpresaAnt;
	}

	public void setDepartamentoEmpresaAnt(String departamentoEmpresaAnt) {
		this.departamentoEmpresaAnt = departamentoEmpresaAnt;
	}

	public String getEmailEmpresaAnt() {
		return emailEmpresaAnt;
	}

	public void setEmailEmpresaAnt(String emailEmpresaAnt) {
		this.emailEmpresaAnt = emailEmpresaAnt;
	}

	public String getTelefonoEmpresaAnt() {
		return telefonoEmpresaAnt;
	}

	public void setTelefonoEmpresaAnt(String telefonoEmpresaAnt) {
		this.telefonoEmpresaAnt = telefonoEmpresaAnt;
	}

	public String getDescripcionEmpresaAnt() {
		return descripcionEmpresaAnt;
	}

	public void setDescripcionEmpresaAnt(String descripcionEmpresaAnt) {
		this.descripcionEmpresaAnt = descripcionEmpresaAnt;
	}

	public String getIdPosicionEmpresaAnt() {
		String posiAnt = null;
		if (posicionEmpresaAnt != null) {
			posiAnt = posicionEmpresaAnt.getIdCatalogo();
		}
		return posiAnt;
	}

	public Catalogo getPosicionEmpresaAnt() {
		return posicionEmpresaAnt;
	}

	public void setPosicionEmpresaAnt(Catalogo posicionEmpresaAnt) {
		this.posicionEmpresaAnt = posicionEmpresaAnt;
	}

	public String getIdSectorEmpresaAnt() {
		String sectorAnt = null;
		if (sectorEmpresaAnt != null) {
			sectorAnt = sectorEmpresaAnt.getIdCatalogo();
		}
		return sectorAnt;
	}

	public Catalogo getSectorEmpresaAnt() {
		return sectorEmpresaAnt;
	}

	public void setSectorEmpresaAnt(Catalogo sectorEmpresaAnt) {
		this.sectorEmpresaAnt = sectorEmpresaAnt;
	}

	public String getIdMesInicioAnt() {
		String mesIn = null;
		if (mesInicioAnt != null) {
			mesIn = mesInicioAnt.getIdCatalogo();
		}
		return mesIn;
	}

	public Catalogo getMesInicioAnt() {
		return mesInicioAnt;
	}

	public void setMesInicioAnt(Catalogo mesInicioAnt) {
		this.mesInicioAnt = mesInicioAnt;
	}

	public String getIdMesFinalAnt() {
		String mesfi = null;
		if (mesFinalAnt != null) {
			mesfi = mesFinalAnt.getIdCatalogo();
		}
		return mesfi;
	}

	public Catalogo getMesFinalAnt() {
		return mesFinalAnt;
	}

	public void setMesFinalAnt(Catalogo mesFinalAnt) {
		this.mesFinalAnt = mesFinalAnt;
	}

	public String getIdAnioInicioAnt() {
		String anioIn = null;
		if (anioInicioAnt != null) {
			anioIn = anioInicioAnt.getIdCatalogo();
		}
		return anioIn;
	}

	public Catalogo getAnioInicioAnt() {
		return anioInicioAnt;
	}

	public void setAnioInicioAnt(Catalogo anioInicioAnt) {
		this.anioInicioAnt = anioInicioAnt;
	}

	public String getIdAnioFinalAnt() {
		String anioFi = null;
		if (anioFinalAnt != null) {
			anioFi = anioFinalAnt.getIdCatalogo();
		}
		return anioFi;
	}

	public Catalogo getAnioFinalAnt() {
		return anioFinalAnt;
	}

	public void setAnioFinalAnt(Catalogo anioFinalAnt) {
		this.anioFinalAnt = anioFinalAnt;
	}

	public String getNombreProyecto() {
		return nombreProyecto;
	}

	public void setNombreProyecto(String nombreProyecto) {
		this.nombreProyecto = nombreProyecto;
	}

	public String getIdCategoriaProyecto() {
		String catePro = null;
		if (categoriaProyecto != null) {
			catePro = categoriaProyecto.getIdCatalogo();
		}
		return catePro;
	}

	public Catalogo getCategoriaProyecto() {
		return categoriaProyecto;
	}

	public void setCategoriaProyecto(Catalogo categoriaProyecto) {
		this.categoriaProyecto = categoriaProyecto;
	}

	public String getIdMesInicioPro() {
		String mesIniPro = null;
		if (mesInicioPro != null) {
			mesIniPro = mesInicioPro.getIdCatalogo();
		}
		return mesIniPro;
	}

	public Catalogo getMesInicioPro() {
		return mesInicioPro;
	}

	public void setMesInicioPro(Catalogo mesInicioPro) {
		this.mesInicioPro = mesInicioPro;
	}

	public String getIdMesFinalPro() {
		String mesFinPro = null;
		if (mesFinalPro != null) {
			mesFinPro = mesFinalPro.getIdCatalogo();
		}
		return mesFinPro;
	}

	public Catalogo getMesFinalPro() {
		return mesFinalPro;
	}

	public void setMesFinalPro(Catalogo mesFinalPro) {
		this.mesFinalPro = mesFinalPro;
	}

	public String getIdAnioInicioPro() {
		String anioIniPro = null;
		if (anioInicioPro != null) {
			anioIniPro = anioInicioPro.getIdCatalogo();
		}
		return anioIniPro;
	}

	public Catalogo getAnioInicioPro() {
		return anioInicioPro;
	}

	public void setAnioInicioPro(Catalogo anioInicioPro) {
		this.anioInicioPro = anioInicioPro;
	}

	public String getIdAnioFinalPro() {
		String anioFinPro = null;
		if (anioFinalPro != null) {
			anioFinPro = anioFinalPro.getIdCatalogo();
		}
		return anioFinPro;
	}

	public Catalogo getAnioFinalPro() {
		return anioFinalPro;
	}

	public void setAnioFinalPro(Catalogo anioFinalPro) {
		this.anioFinalPro = anioFinalPro;
	}

	public String getUrlProyecto() {
		return urlProyecto;
	}

	public void setUrlProyecto(String urlProyecto) {
		this.urlProyecto = urlProyecto;
	}

	public String getDescripcionPro() {
		return descripcionPro;
	}

	public void setDescripcionPro(String descripcionPro) {
		this.descripcionPro = descripcionPro;
	}

	public String getInstitutoPri() {
		return institutoPri;
	}

	public void setInstitutoPri(String institutoPri) {
		this.institutoPri = institutoPri;
	}

	public String getCodigoPostalPri() {
		return codigoPostalPri;
	}

	public void setCodigoPostalPri(String codigoPostalPri) {
		this.codigoPostalPri = codigoPostalPri;
	}

	public String getMunicipioPri() {
		return municipioPri;
	}

	public void setMunicipioPri(String municipioPri) {
		this.municipioPri = municipioPri;
	}

	public String getEstadoPri() {
		return estadoPri;
	}

	public void setEstadoPri(String estadoPri) {
		this.estadoPri = estadoPri;
	}

	public String getPaisPri() {
		return paisPri;
	}

	public void setPaisPri(String paisPri) {
		this.paisPri = paisPri;
	}

	public String getNotaPri() {
		return notaPri;
	}

	public void setNotaPri(String notaPri) {
		this.notaPri = notaPri;
	}

	public String getActividadPri() {
		return actividadPri;
	}

	public void setActividadPri(String actividadPri) {
		this.actividadPri = actividadPri;
	}

	public String getIdTituloPri() {
		String titPri = null;
		if (tituloPri != null) {
			titPri = tituloPri.getIdCatalogo();
		}
		return titPri;
	}

	public Catalogo getTituloPri() {
		return tituloPri;
	}

	public void setTituloPri(Catalogo tituloPri) {
		this.tituloPri = tituloPri;
	}

	public String getIdInicioPri() {
		String iniPri = null;
		if (inicioPri != null) {
			iniPri = inicioPri.getIdCatalogo();
		}
		return iniPri;
	}

	public Catalogo getInicioPri() {
		return inicioPri;
	}

	public void setInicioPri(Catalogo inicioPri) {
		this.inicioPri = inicioPri;
	}

	public String getIdFinPri() {
		String fPri = null;
		if (finPri != null) {
			fPri = finPri.getIdCatalogo();
		}
		return fPri;
	}

	public Catalogo getFinPri() {
		return finPri;
	}

	public void setFinPri(Catalogo finPri) {
		this.finPri = finPri;
	}

	public String getInstitutoSecu() {
		return institutoSecu;
	}

	public void setInstitutoSecu(String institutoSecu) {
		this.institutoSecu = institutoSecu;
	}

	public String getCodigoPostalSecu() {
		return codigoPostalSecu;
	}

	public void setCodigoPostalSecu(String codigoPostalSecu) {
		this.codigoPostalSecu = codigoPostalSecu;
	}

	public String getMunicipioSecu() {
		return municipioSecu;
	}

	public void setMunicipioSecu(String municipioSecu) {
		this.municipioSecu = municipioSecu;
	}

	public String getEstadoSecu() {
		return estadoSecu;
	}

	public void setEstadoSecu(String estadoSecu) {
		this.estadoSecu = estadoSecu;
	}

	public String getPaisSecu() {
		return paisSecu;
	}

	public void setPaisSecu(String paisSecu) {
		this.paisSecu = paisSecu;
	}

	public String getNotaSecu() {
		return notaSecu;
	}

	public void setNotaSecu(String notaSecu) {
		this.notaSecu = notaSecu;
	}

	public String getActividadSecu() {
		return actividadSecu;
	}

	public void setActividadSecu(String actividadSecu) {
		this.actividadSecu = actividadSecu;
	}

	public String getIdTituloSecu() {
		String titsec = null;
		if (tituloSecu != null) {
			titsec = tituloSecu.getIdCatalogo();
		}
		return titsec;
	}

	public Catalogo getTituloSecu() {
		return tituloSecu;
	}

	public void setTituloSecu(Catalogo tituloSecu) {
		this.tituloSecu = tituloSecu;
	}

	public String getIdInicioSecu() {
		String iniSec = null;
		if (inicioSecu != null) {
			iniSec = inicioSecu.getIdCatalogo();
		}
		return iniSec;
	}

	public Catalogo getInicioSecu() {
		return inicioSecu;
	}

	public void setInicioSecu(Catalogo inicioSecu) {
		this.inicioSecu = inicioSecu;
	}

	public String getIdFinSecu() {
		String fSecu = null;
		if (finSecu != null) {
			fSecu = finSecu.getIdCatalogo();
		}
		return fSecu;
	}

	public Catalogo getFinSecu() {
		return finSecu;
	}

	public void setFinSecu(Catalogo finSecu) {
		this.finSecu = finSecu;
	}

	public String getInstitutoPre() {
		return institutoPre;
	}

	public void setInstitutoPre(String institutoPre) {
		this.institutoPre = institutoPre;
	}

	public String getCodigoPostalPre() {
		return codigoPostalPre;
	}

	public void setCodigoPostalPre(String codigoPostalPre) {
		this.codigoPostalPre = codigoPostalPre;
	}

	public String getMunicipioPre() {
		return municipioPre;
	}

	public void setMunicipioPre(String municipioPre) {
		this.municipioPre = municipioPre;
	}

	public String getEstadoPre() {
		return estadoPre;
	}

	public void setEstadoPre(String estadoPre) {
		this.estadoPre = estadoPre;
	}

	public String getPaisPre() {
		return paisPre;
	}

	public void setPaisPre(String paisPre) {
		this.paisPre = paisPre;
	}

	public String getNotaPre() {
		return notaPre;
	}

	public void setNotaPre(String notaPre) {
		this.notaPre = notaPre;
	}

	public String getActividadPre() {
		return actividadPre;
	}

	public void setActividadPre(String actividadPre) {
		this.actividadPre = actividadPre;
	}

	public String getIdTituloPre() {
		String titPre = null;
		if (tituloPre != null) {
			titPre = tituloPre.getIdCatalogo();
		}
		return titPre;
	}

	public Catalogo getTituloPre() {
		return tituloPre;
	}

	public void setTituloPre(Catalogo tituloPre) {
		this.tituloPre = tituloPre;
	}

	public String getIdInicioPre() {
		String iniPre = null;
		if (inicioPre != null) {
			iniPre = inicioPre.getIdCatalogo();
		}
		return iniPre;
	}

	public Catalogo getInicioPre() {
		return inicioPre;
	}

	public void setInicioPre(Catalogo inicioPre) {
		this.inicioPre = inicioPre;
	}

	public String getIdFinPre() {
		String fPre = null;
		if (finSecu != null) {
			fPre = finSecu.getIdCatalogo();
		}
		return fPre;
	}

	public Catalogo getFinPre() {
		return finPre;
	}

	public void setFinPre(Catalogo finPre) {
		this.finPre = finPre;
	}

	public String getInstitutoUni() {
		return institutoUni;
	}

	public void setInstitutoUni(String institutoUni) {
		this.institutoUni = institutoUni;
	}

	public String getCodigoPostalUni() {
		return codigoPostalUni;
	}

	public void setCodigoPostalUni(String codigoPostalUni) {
		this.codigoPostalUni = codigoPostalUni;
	}

	public String getMunicipioUni() {
		return municipioUni;
	}

	public void setMunicipioUni(String municipioUni) {
		this.municipioUni = municipioUni;
	}

	public String getEstadoUni() {
		return estadoUni;
	}

	public void setEstadoUni(String estadoUni) {
		this.estadoUni = estadoUni;
	}

	public String getPaisUni() {
		return paisUni;
	}

	public void setPaisUni(String paisUni) {
		this.paisUni = paisUni;
	}

	public String getNotaUni() {
		return notaUni;
	}

	public void setNotaUni(String notaUni) {
		this.notaUni = notaUni;
	}

	public String getActividadUni() {
		return actividadUni;
	}

	public void setActividadUni(String actividadUni) {
		this.actividadUni = actividadUni;
	}

	public String getIdTituloUni() {
		String titUni = null;
		if (tituloUni != null) {
			titUni = tituloUni.getIdCatalogo();
		}
		return titUni;
	}

	public Catalogo getTituloUni() {
		return tituloUni;
	}

	public void setTituloUni(Catalogo tituloUni) {
		this.tituloUni = tituloUni;
	}

	public String getIdInicioUni() {
		String iniUni = null;
		if (inicioUni != null) {
			iniUni = inicioUni.getIdCatalogo();
		}
		return iniUni;
	}

	public Catalogo getInicioUni() {
		return inicioUni;
	}

	public void setInicioUni(Catalogo inicioUni) {
		this.inicioUni = inicioUni;
	}

	public String getIdFinUni() {
		String fUni = null;
		if (finUni != null) {
			fUni = finUni.getIdCatalogo();
		}
		return fUni;
	}

	public Catalogo getFinUni() {
		return finUni;
	}

	public void setFinUni(Catalogo finUni) {
		this.finUni = finUni;
	}

	public String getInstitutoPos() {
		return institutoPos;
	}

	public String getCodigoPostalPos() {
		return codigoPostalPos;
	}

	public String getMunicipioPos() {
		return municipioPos;
	}

	public String getEstadoPos() {
		return estadoPos;
	}

	public String getPaisPos() {
		return paisPos;
	}

	public String getNotaPos() {
		return notaPos;
	}

	public String getActividadPos() {
		return actividadPos;
	}

	public String getIdTituloPos() {
		String titPos = null;
		if (tituloPos != null) {
			titPos = tituloPos.getIdCatalogo();
		}
		return titPos;
	}

	public Catalogo getTituloPos() {
		return tituloPos;
	}

	public String getIdInicioPos() {
		String iniPos = null;
		if (inicioPos != null) {
			iniPos = inicioPos.getIdCatalogo();
		}
		return iniPos;
	}

	public Catalogo getInicioPos() {
		return inicioPos;
	}

	public String getIdFinPos() {
		String fPos = null;
		if (finPos != null) {
			fPos = finPos.getIdCatalogo();
		}
		return fPos;
	}

	public Catalogo getFinPos() {
		return finPos;
	}

	public void setInstitutoPos(String institutoPos) {
		this.institutoPos = institutoPos;
	}

	public void setCodigoPostalPos(String codigoPostalPos) {
		this.codigoPostalPos = codigoPostalPos;
	}

	public void setMunicipioPos(String municipioPos) {
		this.municipioPos = municipioPos;
	}

	public void setEstadoPos(String estadoPos) {
		this.estadoPos = estadoPos;
	}

	public void setPaisPos(String paisPos) {
		this.paisPos = paisPos;
	}

	public void setNotaPos(String notaPos) {
		this.notaPos = notaPos;
	}

	public void setActividadPos(String actividadPos) {
		this.actividadPos = actividadPos;
	}

	public void setTituloPos(Catalogo tituloPos) {
		this.tituloPos = tituloPos;
	}

	public void setInicioPos(Catalogo inicioPos) {
		this.inicioPos = inicioPos;
	}

	public void setFinPos(Catalogo finPos) {
		this.finPos = finPos;
	}

	public String getInstitutoDoc() {
		return institutoDoc;
	}

	public String getCodigoPostalDoc() {
		return codigoPostalDoc;
	}

	public String getMunicipioDoc() {
		return municipioDoc;
	}

	public String getEstadoDoc() {
		return estadoDoc;
	}

	public String getPaisDoc() {
		return paisDoc;
	}

	public String getNotaDoc() {
		return notaDoc;
	}

	public String getActividadDoc() {
		return actividadDoc;
	}

	public String getIdTituloDoc() {
		String titDoc = null;
		if (tituloDoc != null) {
			titDoc = tituloDoc.getIdCatalogo();
		}
		return titDoc;
	}

	public Catalogo getTituloDoc() {
		return tituloDoc;
	}

	public String getIdInicioDoc() {
		String iniDoc = null;
		if (inicioDoc != null) {
			iniDoc = inicioDoc.getIdCatalogo();
		}
		return iniDoc;
	}

	public Catalogo getInicioDoc() {
		return inicioDoc;
	}

	public String getIdFinDoc() {
		String fDoc = null;
		if (finDoc != null) {
			fDoc = finDoc.getIdCatalogo();
		}
		return fDoc;
	}

	public Catalogo getFinDoc() {
		return finDoc;
	}

	public void setInstitutoDoc(String institutoDoc) {
		this.institutoDoc = institutoDoc;
	}

	public void setCodigoPostalDoc(String codigoPostalDoc) {
		this.codigoPostalDoc = codigoPostalDoc;
	}

	public void setMunicipioDoc(String municipioDoc) {
		this.municipioDoc = municipioDoc;
	}

	public void setEstadoDoc(String estadoDoc) {
		this.estadoDoc = estadoDoc;
	}

	public void setPaisDoc(String paisDoc) {
		this.paisDoc = paisDoc;
	}

	public void setNotaDoc(String notaDoc) {
		this.notaDoc = notaDoc;
	}

	public void setActividadDoc(String actividadDoc) {
		this.actividadDoc = actividadDoc;
	}

	public void setTituloDoc(Catalogo tituloDoc) {
		this.tituloDoc = tituloDoc;
	}

	public void setInicioDoc(Catalogo inicioDoc) {
		this.inicioDoc = inicioDoc;
	}

	public void setFinDoc(Catalogo finDoc) {
		this.finDoc = finDoc;
	}

	public String getNomberCertificacion() {
		return nomberCertificacion;
	}

	public String getUrlCertificacion() {
		return urlCertificacion;
	}

	public String getClaveCertificacion() {
		return claveCertificacion;
	}

	public String getIdCursoCertificacion() {
		String cursoCer = null;
		if (cursoCertificacion != null) {
			cursoCer = cursoCertificacion.getIdCatalogo();
		}
		return cursoCer;
	}

	public Catalogo getCursoCertificacion() {
		return cursoCertificacion;
	}

	public String getIdAnioInicioCertificacion() {
		String anioCert = null;
		if (anioInicioCertificacion != null) {
			anioCert = anioInicioCertificacion.getIdCatalogo();
		}
		return anioCert;
	}

	public Catalogo getAnioInicioCertificacion() {
		return anioInicioCertificacion;
	}

	public String getIdAnioFinalCertificacion() {
		String anioFinCert = null;
		if (anioFinalCertificacion != null) {
			anioFinCert = anioFinalCertificacion.getIdCatalogo();
		}
		return anioFinCert;
	}

	public Catalogo getAnioFinalCertificacion() {
		return anioFinalCertificacion;
	}

	public void setNomberCertificacion(String nomberCertificacion) {
		this.nomberCertificacion = nomberCertificacion;
	}

	public void setUrlCertificacion(String urlCertificacion) {
		this.urlCertificacion = urlCertificacion;
	}

	public void setClaveCertificacion(String claveCertificacion) {
		this.claveCertificacion = claveCertificacion;
	}

	public void setCursoCertificacion(Catalogo cursoCertificacion) {
		this.cursoCertificacion = cursoCertificacion;
	}

	public void setAnioInicioCertificacion(Catalogo anioInicioCertificacion) {
		this.anioInicioCertificacion = anioInicioCertificacion;
	}

	public void setAnioFinalCertificacion(Catalogo anioFinalCertificacion) {
		this.anioFinalCertificacion = anioFinalCertificacion;
	}

	public String getNombreCurso() {
		return nombreCurso;
	}

	public String getFolioCurso() {
		return folioCurso;
	}

	public String getUrlCurso() {
		return urlCurso;
	}

	public String getIdDocumentoCurso() {
		String docuCurso = null;
		if (documentoCurso != null) {
			docuCurso = documentoCurso.getIdCatalogo();
		}
		return docuCurso;
	}

	public Catalogo getDocumentoCurso() {
		return documentoCurso;
	}

	public String getIdMesInicioCurso() {
		String mesInCurso = null;
		if (mesInicioCurso != null) {
			mesInCurso = mesInicioCurso.getIdCatalogo();
		}
		return mesInCurso;
	}

	public Catalogo getMesInicioCurso() {
		return mesInicioCurso;
	}

	public String getIdMesFinalCurso() {
		String mesFinCursos = null;
		if (mesFinalCurso != null) {
			mesFinCursos = mesFinalCurso.getIdCatalogo();
		}
		return mesFinCursos;
	}

	public Catalogo getMesFinalCurso() {
		return mesFinalCurso;
	}

	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}

	public void setFolioCurso(String folioCurso) {
		this.folioCurso = folioCurso;
	}

	public void setUrlCurso(String urlCurso) {
		this.urlCurso = urlCurso;
	}

	public void setDocumentoCurso(Catalogo documentoCurso) {
		this.documentoCurso = documentoCurso;
	}

	public void setMesInicioCurso(Catalogo mesInicioCurso) {
		this.mesInicioCurso = mesInicioCurso;
	}

	public void setMesFinalCurso(Catalogo mesFinalCurso) {
		this.mesFinalCurso = mesFinalCurso;
	}

	public String getTipoPremio() {
		return tipoPremio;
	}

	public String getEmisorPremio() {
		return emisorPremio;
	}

	public String getDescripcionPremio() {
		return descripcionPremio;
	}

	public String getIdAnioPremio() {
		String anioPre = null;
		if (anioPremio != null) {
			anioPre = anioPremio.getIdCatalogo();
		}
		return anioPre;
	}

	public Catalogo getAnioPremio() {
		return anioPremio;
	}

	public void setTipoPremio(String tipoPremio) {
		this.tipoPremio = tipoPremio;
	}

	public void setEmisorPremio(String emisorPremio) {
		this.emisorPremio = emisorPremio;
	}

	public void setDescripcionPremio(String descripcionPremio) {
		this.descripcionPremio = descripcionPremio;
	}

	public void setAnioPremio(Catalogo anioPremio) {
		this.anioPremio = anioPremio;
	}

	public String getNombreIdioma() {
		return nombreIdioma;
	}

	public String getDescripcionAptitudes() {
		return descripcionAptitudes;
	}

	public String getIdOralIdioma() {
		String oralIdio = null;
		if (oralIdioma != null) {
			oralIdio = oralIdioma.getIdCatalogo();
		}
		return oralIdio;
	}
	
	public Catalogo getOralIdioma() {
		return oralIdioma;
	}

	public String getIdEscritoIdioma() {
		String escritoIdio = null;
		if (escritoIdioma != null) {
			escritoIdio = escritoIdioma.getIdCatalogo();
		}
		return escritoIdio;
	}
	
	public Catalogo getEscritoIdioma() {
		return escritoIdioma;
	}

	public String getIdComprendidoIdioma() {
		String comprenIdio = null;
		if (comprendidoIdioma != null) {
			comprenIdio = comprendidoIdioma.getIdCatalogo();
		}
		return comprenIdio;
	}
	
	public Catalogo getComprendidoIdioma() {
		return comprendidoIdioma;
	}

	public void setNombreIdioma(String nombreIdioma) {
		this.nombreIdioma = nombreIdioma;
	}

	public void setDescripcionAptitudes(String descripcionAptitudes) {
		this.descripcionAptitudes = descripcionAptitudes;
	}

	public void setOralIdioma(Catalogo oralIdioma) {
		this.oralIdioma = oralIdioma;
	}

	public void setEscritoIdioma(Catalogo escritoIdioma) {
		this.escritoIdioma = escritoIdioma;
	}

	public void setComprendidoIdioma(Catalogo comprendidoIdioma) {
		this.comprendidoIdioma = comprendidoIdioma;
	}
	
}
