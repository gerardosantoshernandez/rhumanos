package com.crenx.data.domain.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class PropiedadesDoc {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_propiedades_doc", unique = true)
	private String idPropiedadesDoc;
	
	@Column(name = "nombre",length = 255)
	private String nombre;

	@Column(name = "valor",length = 255)
	private String valor; 
	
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_documento",insertable=true,updatable=false,referencedColumnName="id_documento")
	private Documento documento;

	/**
	 * @return the documento
	 */
	public Documento getDocumento() {
		return documento;
	}

	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public String getIdPropiedadesDoc() {
		return idPropiedadesDoc;
	}

	public void setIdPropiedadesDoc(String idPropiedadesDoc) {
		this.idPropiedadesDoc = idPropiedadesDoc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	
}
