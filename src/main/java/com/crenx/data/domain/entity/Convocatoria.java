package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Convocatoria {

	@Id @GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	@Column(name = "id_convocatoria", unique = true)
	private String idConvocatoria;
	
	@Column(name = "nombre",length = 100)
	private String nombre;
	
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_empleado", nullable = true, referencedColumnName = "id_empleado")
	private Empleado idEmpleado;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo_convocatoria", nullable = false, referencedColumnName = "id_catalogo")
	private Catalogo idTipoConvocatoria;

	public String getIdConvocatoria() {
		return idConvocatoria;
	}

	public void setIdConvocatoria(String idConvocatoria) {
		this.idConvocatoria = idConvocatoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Empleado getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Empleado idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public Catalogo getIdTipoConvocatoria() {
		return idTipoConvocatoria;
	}

	public void setIdTipoConvocatoria(Catalogo idTipoConvocatoria) {
		this.idTipoConvocatoria = idTipoConvocatoria;
	}
	
	
	
}
