package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Anuncio {
	
	@Id @GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "id_anuncio", unique = true)
	private String idAnuncio;
	
	@Column(name = "asunto",length = 100)
	private String asunto;
	
	@Column(name = "descripcion", length = 200)
	private String descripcion;
	
	@Column(name = "fecha_anuncio")
	private Date fechaAviso;
	
	@Column(name = "fecha_ausente")
	private Date fechaAusente;
	
	@Column(name = "fecha_llegada")
	private Date fechaRegreso;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_empleado", nullable = false, referencedColumnName = "id_empleado")
	private Empleado idEmpleado;

	public String getIdAnuncio() {
		return idAnuncio;
	}

	public void setIdAnuncio(String idAnuncio) {
		this.idAnuncio = idAnuncio;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaAviso() {
		return fechaAviso;
	}

	public void setFechaAviso(Date fechaAviso) {
		this.fechaAviso = fechaAviso;
	}

	public Date getFechaAusente() {
		return fechaAusente;
	}

	public void setFechaAusente(Date fechaAusente) {
		this.fechaAusente = fechaAusente;
	}

	public Date getFechaRegreso() {
		return fechaRegreso;
	}

	public void setFechaRegreso(Date fechaRegreso) {
		this.fechaRegreso = fechaRegreso;
	}

	public Empleado getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Empleado idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	
}
