'use strict';
angular.module('crenx.processcontrollers', [])
.controller('TaskLoadController', function ($scope, $rootScope, apiService, $stateParams, $state, $timeout, $modal) {	
	var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

    function loadTasks()
    {
	    apiService.get('/api/process/mistareas', null,
	            taskCompleted,
	            taskFailed);	    	
    }
    
    function taskFailed(response) {
        toastr.error(response.data.sourceMessage, "Error", opts);
    }
    function taskCompleted(response) {
    	$scope.tasks = response.data;
    	$scope.taskSize = response.data.length;
    }
    
    $scope.executeTask = function(taskId)
    {
    	apiService.get('/api/process/taskDetails/' + taskId, null,
    				completeTask,
    				taskFailed);
    }

    function completeTask(response)
    {
    	var taskDefinitionKey = response.data.formKey;
    	var state = 'app.' + taskDefinitionKey; 
        $state.go(state, {task: response.data});
    }      
    loadTasks();

})
.controller('TaskController', function ($scope, $rootScope, apiService, $stateParams, $state, $timeout, $modal) {
	$scope.taskId = $stateParams.taskId;


	var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
	
    apiService.get('/runtime/tasks/' + $scope.taskId, null,
            taskCompleted,
            taskFailed);
    function taskFailed(response) {
        toastr.error(response.data.sourceMessage, "Error", opts);
    }
    function taskCompleted(response) {
    	$scope.taskInfo = response.data;
    }
})
.controller('ApproveCreditoController', function ($scope, $rootScope, apiService, $stateParams, $state, $timeout, $modal,documentService) {
	$scope.taskId = $stateParams.task.id;
	$rootScope.currentDocument = null;
	$rootScope.allowDownload= !($rootScope.mobile_interface);
	$rootScope.cleanEnvDocuments();

	$scope.disabledButtons = false;
	var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
	
    apiService.get('/runtime/tasks/' + $scope.taskId, null,
            taskLoaded,
            taskFailed);
    apiService.get('/runtime/tasks/' + $scope.taskId +'/variables', null,
            varLoaded,
            taskFailed);
    function taskFailed(response) {
        toastr.error(response.data.sourceMessage, "Error", opts);
    }
    function taskLoaded(response) {
    	$scope.taskInfo = response.data;
    }
    function varLoaded(response) {
    	var varName="";
    	var varValue = ""
    	for (var i=0; i<response.data.length; i++)
		{
    		varName = response.data[i].name;
    		if (varName=='idCredito')
			{
    			varValue = response.data[i].value;
    			break;
			}
		}
    	
    	apiService.post("/api/cliente/detalleCredito", varValue, loadCredito, taskFailed);
    	    	
    }
    function loadCredito(response)
    {
    	$scope.credito = response.data;    	
    	//Se buscan los documentos asociados al cliente
    	var filtro = {};     	
    	filtro.idCliente = $scope.credito.idCliente;
    	
    	//Documentos del cliente
    	apiService.post('/api/documentos/obtenerDoctosPorFiltro', filtro,
    			function(result){    					
			    		if (result.data && result.data.length > 0) {			    			
			    			$scope.credito.documentos = result.data;
						}
    				},
    				function(response){
    					toastr.error(response.data.sourceMessage, "No se pueden cargar los documentos asociados al cliente", opts);
        			  });
    	//Documentos del codeudor
    	if($scope.credito.idCoDedudor && $scope.credito.idCoDedudor != ''){
    		var filtroCo = {};
        	filtroCo.idCliente = $scope.credito.idCoDedudor;
        	apiService.post('/api/documentos/obtenerDoctosPorFiltro', filtroCo,
        			function(result){    					
    			    		if (result.data && result.data.length > 0) {    			    			
    			    			$scope.credito.doctosCodeudor = result.data;
    						}
        				},
        				function(response){
        					toastr.error(response.data.sourceMessage, "No se pueden cargar los documentos asociados al codeudor", opts);
            			  });
    	}    	    	
    }
    
    $scope.submitTheForm = function (approved) {
    	$scope.disabledButtons = true;
    	
    	$scope.credito.aprobado = approved;
    	$scope.credito.taskId = $scope.taskId;
        apiService.post('/api/cliente/aprobarcredito', $scope.credito,
        		aprobarSuccess,
                taskFailed);
    }

    function aprobarSuccess(response)
    {
        toastr.success("Se completó la operación", "Aprobar crédito", opts);
        $state.go('app.tareas');
    }
    
	  /**
	  * Función encargada de la descarga del documento 
	  * Usa la función global $rootScope.displayDocument
	  */
	 $scope.display = function (docto, modal_id){
		 $rootScope.currentDocument = docto;
		 var url_location = "/api/documentos/consultarDocumento/"+docto.idDocumento;
		 $rootScope.displayDocument(url_location, modal_id);				 
	 };
	
}).
controller('ApproveGastoController', function ($scope, $rootScope, apiService, $stateParams, $state, $timeout, $modal,documentService) {
	$scope.taskId = $stateParams.task.id;
	$rootScope.allowDownload= !($rootScope.mobile_interface);
	$scope.disabledButtons = false;
	$rootScope.cleanEnvDocuments();
	$rootScope.allowDownload= !($rootScope.mobile_interface);
	var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
	
    apiService.get('/runtime/tasks/' + $scope.taskId, null,
            taskLoaded,
            taskFailed);
    apiService.get('/runtime/tasks/' + $scope.taskId +'/variables', null,
            varLoaded,
            taskFailed);
    function taskFailed(response) {
        toastr.error(response.data.sourceMessage, "Error", opts);
    }
    function taskLoaded(response) {
    	$scope.taskInfo = response.data;
    }
    function varLoaded(response) {
    	var varName="";
    	var varValue="";
    	for (var i=0; i<response.data.length; i++)
		{
    		varName = response.data[i].name;
    		if (varName=='IdMC')
			{
    			varValue = response.data[i].value;
    			break;
			}
		}
    	apiService.post("/api/org/loadGasto", varValue, loadGasto, taskFailed)
    }
    function loadGasto(response)
    {
    	$scope.mc = response.data;
    	//Se buscan los documentos asociados al gasto
    	var filtro = {};
    	filtro.idMovCajaGasto = $scope.mc.idMC;    	
    	
    	apiService.post('/api/documentos/obtenerDoctosPorFiltro', filtro,
    			function(result){ 
    					$scope.buttondocto = (result.data && result.data.length > 0);
			    		if ($scope.buttondocto) {
			    			$scope.documento = result.data[0];
						}
    				},
    				function(response){
    				$scope.buttondocto = false;
    					toastr.error(response.data.sourceMessage, "No se pueden cargar el documento asociado al gasto", opts);        			  
        			 });
    }
    
    $scope.submitTheForm = function (approved) {
    	$scope.disabledButtons = true;    	
    	$scope.mc.aprobado = approved;
    	$scope.mc.taskId = $scope.taskId;
        apiService.post('/api/org/aprobargasto', $scope.mc,
        		aprobarSuccess,
                taskFailed);
    }

    function aprobarSuccess(response)
    {
        toastr.success("Se completó la operación", "Aprobar crédito", opts);
        $state.go('app.tareas');
    }
    
    /**
	  * Función encargada de la descarga del documento 
	  * Usa la función global $rootScope.displayDocument
	  */
	 $scope.display = function (docto, modal_id){
		 $rootScope.currentDocument = docto;
		 var url_location = "/api/documentos/consultarDocumento/"+docto.idDocumento;
		 $rootScope.displayDocument(url_location, modal_id);				 
	 };
}).
controller('ApproveTransferController', function ($scope, $rootScope, apiService, $stateParams, $state, $timeout, $modal,documentService) {
	$scope.taskId = $stateParams.task.id;
	$scope.monto = 0;
	$rootScope.cleanEnvDocuments();
	$rootScope.allowDownload= !($rootScope.mobile_interface);
	
	$scope.disabledButtons = false;
	var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    apiService.get('/runtime/tasks/' + $scope.taskId, null,
            taskLoaded,
            taskFailed);
	
    apiService.get('/runtime/tasks/' + $scope.taskId +'/variables', null,
            varLoaded,
            taskFailed);
    function taskLoaded(response) {
    	$scope.taskInfo = response.data;
    }
    function taskFailed(response) {
        toastr.error(response.data.sourceMessage, "Error", opts);
    }
    function varLoaded(response) {
    	for (var i=0; i<response.data.length; i++)
		{
    		var varName = response.data[i].name;
    		if (varName=='origen')
			{
    			$scope.origen = response.data[i].value;
			}
    		if (varName=='destino')
			{
    			$scope.destino = response.data[i].value;
			}
    		if (varName=='monto')
			{
    			$scope.monto = response.data[i].value;
			}
    		if (varName=='fechaTransfer')
			{
    			$scope.fechaTransfer = response.data[i].value;
			}
    		if (varName=='mcOrigen')
			{
    			$scope.mcOrigen = response.data[i].value;
			}
    		if (varName=='mcDestino')
			{
    			$scope.mcDestino = response.data[i].value;
			}
		}
    	
    	//Se buscan los documentos asociados al gasto
    	var filtro = {};
    	filtro.idMovCajaTrasferencia = $scope.mcOrigen;    	
    	
    	apiService.post('/api/documentos/obtenerDoctosPorFiltro', filtro,
    			function(result){ 
    					$scope.buttondocto = (result.data && result.data.length > 0);
			    		if ($scope.buttondocto) {
			    			$scope.documento = result.data[0];
						}
    				},
    				function(response){
    				$scope.buttondocto = false;
        			  toastr.error(response.data.sourceMessage, "No se pueden cargar el documento asociado a la transferencia", opts);
        			  });
    }
    
    $scope.submitTheForm = function (approved) {
    	$scope.disabledButtons = true;
    	
    	var mc = {aprobado:approved, taskId:$scope.taskId, idMC:$scope.mcOrigen, idMCDestino:$scope.mcDestino}
        apiService.post('/api/org/aprobarTransfer', mc,
        		aprobarSuccess,
                taskFailed);
    }

    function aprobarSuccess(response)
    {
        toastr.success("Se completó la operación", "Aprobar transferencia", opts);
        $state.go('app.tareas');
    }
    
    
    /**
	  * Función encargada de la descarga del documento 
	  * Usa la función global $rootScope.displayDocument
	  */
	 $scope.display = function (docto, modal_id){
		 $rootScope.currentDocument = docto;
		 var url_location = "/api/documentos/consultarDocumento/"+docto.idDocumento;
		 $rootScope.displayDocument(url_location, modal_id);				 
	 };
})


;
