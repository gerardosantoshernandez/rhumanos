	'use strict';
	angular.module('crenx.convocatoriacontrollers', [])
	.controller(
					'ConvocatoriaController',
					function($scope, $rootScope, apiService, $modal, $timeout,
							$state) {
						$scope.allUsers = [];
						$scope.filters = {
							roles : [],
							estatus : []
						};
						$scope.tipoObjeto = $state.current.params.tipoObjeto;

						var filterTextTimeout;
						$scope.$watch('filters.correoElectronico', function(val) {
							if (filterTextTimeout)
								$timeout.cancel(filterTextTimeout);
							filterTextTimeout = $timeout(function() {
								loadData();
							}, 600); // delay 250 ms
						})

						$scope.$watch('filters.nombre', function(val) {
							if (filterTextTimeout)
								$timeout.cancel(filterTextTimeout);
							filterTextTimeout = $timeout(function() {
								loadData();
							}, 300); // delay 250 ms
						})

						$scope.$watch('filters.apellido', function(val) {
							if (filterTextTimeout)
								$timeout.cancel(filterTextTimeout);
							filterTextTimeout = $timeout(function() {
								loadData();
							}, 300); // delay 250 ms
						})

						// Open Simple Modal
						$scope.openModal = function(modal_id, modal_size, userid,
								accion) {
							$rootScope.userid = userid;
							$rootScope.accion = accion;
							$rootScope.currentModal = $modal
									.open({
										templateUrl : modal_id,
										size : modal_size,
										backdrop : typeof modal_backdrop == 'undefined' ? true
												: modal_backdrop
									});
						};

						$rootScope.resetPwd = {
							idUsuario : null,
							contrasena : null
						};
						$scope.resetPassword = function(modal_id, modal_size,
								userid, userName) {
							$rootScope.userid = userid;
							$rootScope.userName = userName;
							$rootScope.resetPwd.idUsuario = userid;

							$rootScope.currentModal = $modal
									.open({
										templateUrl : modal_id,
										size : modal_size,
										backdrop : typeof modal_backdrop == 'undefined' ? true
												: modal_backdrop
									});
						};

						var opts = {
							"closeButton" : true,
							"debug" : false,
							"positionClass" : "toast-top-right",
							"onclick" : null,
							"showDuration" : "300",
							"hideDuration" : "1000",
							"timeOut" : "5000",
							"extendedTimeOut" : "1000",
							"showEasing" : "swing",
							"hideEasing" : "linear",
							"showMethod" : "fadeIn",
							"hideMethod" : "fadeOut"
						};

						jQuery(document)
								.ready(
										function($) {
											$("#rolefilter")
													.select2(
															{
																placeholder : 'Tipo de usuario',
																allowClear : true
															})
													.on(
															'select2-open',
															function() {
																$(this).data(
																		'select2').results
																		.addClass(
																				'overflow-hidden')
																		.perfectScrollbar();
															}).on('change',
															function(e) {
																changeRol(e);
															})

											$("#estatusfilter")
													.select2(
															{
																placeholder : 'Estatus de usuario',
																allowClear : true
															})
													.on(
															'select2-open',
															function() {
																$(this).data(
																		'select2').results
																		.addClass(
																				'overflow-hidden')
																		.perfectScrollbar();
															}).on('change',
															function(e) {
																changeEstatus(e);
															})

										});

						function loadData() {
							apiService.post('/api/cuenta', $scope.filters,
									lcoationLoadCompleted, serviceError);

						}

						function addSelectItems(selectControl, listaItems) {
							$.each(listaItems, function(key, value) {
								$(selectControl).append(
										$("<option></option>").val(
												value.idObjetoSeguridad).text(
												value.nombre));
							});

						}
						function addSelectItems2(selectControl, listaItems) {
							$.each(listaItems, function(key, value) {
								$(selectControl).append(
										$("<option></option>")
												.val(value.idCatalogo).text(
														value.nombre));
							});

						}

						function loadRole() {
							apiService.post('/api/cuenta/objetosseguridad',
									$scope.tipoObjeto,
									function lcoationLoadCompleted(result) {
										$scope.allRoles = result.data;
										addSelectItems("#rolefilter",
												$scope.allRoles);
									}, serviceError);

						}
						function loadEstatus() {
							apiService.post('/api/catalogo/items',
									'CAT_ESTATUS_GEN', function loadOk(result) {
										$scope.estatus = result.data.items;
										addSelectItems2("#estatusfilter",
												$scope.estatus);
									}, serviceError);
						}

						function lcoationLoadCompleted(result) {
							$scope.allUsers = result.data;

						}

						function serviceError(response) {
							toastr.error("Sucedión un error", "Error", opts);
						}

						function changeRol(e) {
							$scope.filters.roles = e.val;
							loadData();
						}
						;

						function changeEstatus(e) {
							$scope.filters.estatus = e.val;
							loadData();
						}
						;

						loadRole();
						loadEstatus();
						loadData();

						$rootScope.resetPassword = function() {
							apiService
									.post(
											'/api/cuenta/resetPassword/',
											$rootScope.resetPwd,
											function(result) {
												toastr
														.success(
																"Se eliminó satisfactoriamente el ususrio.",
																"Eliminar usuario",
																opts);
												$rootScope.currentModal.close();
											}, serviceError);
						}
						$rootScope.deleteUser = function(item, event) {
							apiService
									.post(
											'/api/cuenta/inactivaUsuario/',
											$rootScope.userid,
											function(result) {
												$scope.questionary = result.data;
												var mensaje = "";
												if ($rootScope.accion == 'Activar')
													mensaje = "activo";
												else
													mensaje = "desactivo";
												toastr
														.success(
																"Se "
																		+ mensaje
																		+ " satisfactoriamente el usuario.",
																"Cambiar estatus",
																opts);
												$rootScope.currentModal.close();
												loadData();
											}, serviceError);
						};

						$scope.showRoles = function(nombreUsuario, modal_id,
								modal_size, modal_backdrop) {
							// modal
							loadRolesUsuario(nombreUsuario);
							// $rootScope.motivo ="";
							$rootScope.currentModal = $modal
									.open({
										templateUrl : 'showRoles',
										size : modal_size,
										backdrop : typeof modal_backdrop == 'undefined' ? true
												: modal_backdrop
									});

						};

						function loadRolesUsuario(nombreUsuario) {
							apiService.post('/api/cuenta/getRolesUsuario',
									nombreUsuario, function(result) {
										$rootScope.roles = result.data;
									}, serviceError);
						}

					});