'use strict';

angular.module('crenx.indicadorescontrollers', []).
controller('CarteraController', function ($scope, $rootScope, apiService, $stateParams, $state, $timeout) {
    $scope.filters = { divisiones: [], regiones: [], gerencias: [], rutas: [], nombre:null, apellidoPaterno:null, periodo:"dia" };
	$scope.clientes = [];
	$scope.orgId = $rootScope.repository.loggedUser.idTitOrg==null?"null":$rootScope.repository.loggedUser.idTitOrg;
	//Se guarda en el Scope principal si los indicadores fuerón invocados en mobile o no
	//mobile_interface es un parametro en la URL cuyos valores son : true o false
	//Este parámetro es útil en varias condiciones en la aplicación
	$rootScope.mobile_interface = ($stateParams.mobile_interface)?$stateParams.mobile_interface:false;
	//Estos catálogos se deben llenar aplicando los filtros del usuario, una sola vez
	$scope.divisiones = [];
	$scope.regiones = [];
	$scope.gerencias = [];
	$scope.rutas = [];
	$scope.cliente = {};
	$scope.countCreditos = 150;
	$scope.periodos = [{idPeriodo:"dia", nombre:"Diario"}, {idPeriodo:"mes", nombre:"Mes"}, {idPeriodo:"anio", nombre:"Año"}];
	var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
	getDateFilter();
	loadData();
	
	
	
    function getDateFilter() {
        apiService.post('/api/indicadores/fechasSemana', $scope.filters,
                    function (result)
                    {
        				$scope.filters.fechaInicial = result.data.sDesde;
        				$scope.filters.fechaFinal = result.data.sHasta;
                    },
                    locationLoadFailed);
    }

	function prepareListItems(control, placeHolderVal)
	{
		$(control)
		.select2(
		{
			placeholder : placeHolderVal,
			allowClear : true
		})
		.on(
			'select2-open', function() {
				// Adding Custom Scrollbar
				$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		}).on('change', function (e) 
				{
					var source = $(this).context.id;
					if (source=='ruta')
					{
						$scope.filters.rutas = [];
						for (var i=0; i<e.val.length; i++)
						{
							$scope.filters.rutas.push({id:e.val[i], name:"no importa"})
						}
					}else
					{
						if (source=='gerencia')
						{
							$scope.filters.gerencias = [];
							for (var i=0; i<e.val.length; i++)
							{
								$scope.filters.gerencias.push({id:e.val[i], name:"no importa"})
							}
						}
						if (source=='region')
						{
							$scope.filters.regiones = [];
							for (var i=0; i<e.val.length; i++)
							{
								$scope.filters.regiones.push({id:e.val[i], name:"no importa"})
							}
						}
						if (source=='division')
						{
							$scope.filters.divisiones = [];
							for (var i=0; i<e.val.length; i++)
							{
								$scope.filters.divisiones.push({id:e.val[i], name:"no importa"})
							}
						}
						loadFilteredTree(e.val, source)									
					}
					loadData();
		});		
	}
    
	jQuery(document)
	.ready(
	function($) {
		prepareListItems("#division", 'División');
		prepareListItems("#region", 'Región');
		prepareListItems("#gerencia", 'Gerencia');
		prepareListItems("#ruta", 'Ruta');
	});
    
        
	function addSelectItems(selectControl, listaItems)
	{
		$(selectControl).empty();
    	$.each(listaItems, function(key, value) {   
   	     $(selectControl)
   	         .append($("<option></option>")
   	         .val(value.id)
   	         .text(value.name)); 
    	});
		
	}
    function loadOrgTree() {
        apiService.post('/api/org/filteredTree', $scope.orgId,
                    function (result)
                    {
        				$scope.divisiones = result.data.divisiones;
			        	$scope.regiones = result.data.regiones;
			        	$scope.gerencias = result.data.gerencias;
			        	$scope.rutas = result.data.rutas;
        				addSelectItems("#division",$scope.divisiones);
        				addSelectItems("#region",$scope.regiones);
        				addSelectItems("#gerencia",$scope.gerencias);
        				addSelectItems("#ruta",$scope.rutas);
                    },
                    locationLoadFailed);

    }
    
    
    function loadFilteredTree(filters, nivel)
    {
    	var filtros = {};
    	if (nivel=='division')
		{
    		filtros.tipoNodo = 'D';
		}
    	if (nivel=='region')
		{
    		filtros.tipoNodo = 'R';
		}
    	if (nivel=='gerencia')
		{
    		filtros.tipoNodo = 'G';
		}
    	if (nivel=='ruta')
		{
    		filtros.tipoNodo = 'RR';
		}
    	filtros.filtros = filters;
        apiService.post('/api/org/limitedTree', filtros,
                function (result)
                {
        			if (nivel=='division')
    				{
        				addSelectItems("#region",result.data.regiones);
        				addSelectItems("#gerencia",result.data.gerencias);
        				addSelectItems("#ruta",result.data.rutas);        				
    				}
        			if (nivel=='region')
    				{
        				addSelectItems("#gerencia",result.data.gerencias);
        				addSelectItems("#ruta",result.data.rutas);        				
    				}
        			if (nivel=='gerencia')
    				{
        				addSelectItems("#ruta",result.data.rutas);        				
    				}
                },
                locationLoadFailed);    	
    }

    function loadData()
    {
    	if ($scope.orgId!='null')
		{
    		$scope.filters.rutas.push({id:$scope.orgId, name:"no importa"})
		}
        apiService.post('/api/indicadores/carteratotal', $scope.filters,
        		function(result)
        		{
        			$scope.carteraDS = result.data.carteraDia;
        			$scope.indicadores = $scope.carteraDS[$scope.carteraDS.length-1];
        			$scope.countCreditos = result.data.cartera.countCreditos;
        			$scope.totalCartera = $scope.indicadores.creditoTotalImp;
        			$scope.carteraCapital = $scope.indicadores.carteraCapital;
        			$scope.countCreditosVen = $scope.indicadores.creditosVencidos;
        			$scope.totalCarteraVen = $scope.indicadores.creditosVencidosImp;
        			$scope.countCreditosNvo = $scope.indicadores.creditosNuevos;
        			$scope.countClientesNvo = result.data.clientesNuevos.countCreditos;
        			$scope.countClientesTot = result.data.clientesTotales.countCreditos;
        			$scope.countClientesAct = result.data.clientesActivos.countCreditos;
        			$scope.normalidadData = [];
        			$scope.cobranzaData = [];
        			var i=0;
        			var cobranza=0;
        			var normalidad=0;
        			$scope.factorNormalidad = 0;
        			$scope.factorCobranza = 0;        			
        			for (; i<result.data.indicadoresCobranza.length;i++)
    				{
        				var newDate = new Date(result.data.indicadoresCobranza[i].fecha);
        				$scope.normalidadData.push({time: newDate, norms: result.data.indicadoresCobranza[i].normalidad  });
        				$scope.cobranzaData.push({time: newDate, cobs: result.data.indicadoresCobranza[i].cobranza  });
        				cobranza+=result.data.indicadoresCobranza[i].cobranza;
        				normalidad+=result.data.indicadoresCobranza[i].normalidad;
    				}
        			if (i>0)
    				{
        				$scope.factorNormalidad = Math.round(normalidad/i);
        				$scope.factorCobranza = Math.round(cobranza/i);
    				}
        			renderData();
        		},
                locationLoadFailed);
    }
    function lcoationLoadCompleted(result) {
        $scope.clientes = result.data;
    }

    function locationLoadFailed(response) {
    	toastr.error(response.data.sourceMessage, "Error al cargar los indicaores", opts);
    }
        
    loadOrgTree();
    
    
    function renderData()
    		{
    			var $ = jQuery;		
    			if( ! $.isFunction($.fn.dxChart))
    				return;
    				
    			var gaugesPalette = ['#8dc63f', '#40bbea', '#ffba00', '#cc3f44'];    			
    				
    			var normalidad_data = $scope.normalidadData;
    			
    			var cobranza_data = $scope.cobranzaData;

				var dataSource = $scope.carteraDS;
				$("#bar-3")
				.dxChart(
						{
							dataSource : dataSource,
							commonSeriesSettings : {
								argumentField : "dia",
								type: "fullstackedbar"
							},
							series : [
									{
										valueField : "creditoNormalImp",
										name : "Normal",
										color : "#50c63f"
									},
									{
										valueField : "creditoCorrectivoImp",
										name : "Correctivo",
										color : "#FFB533"
									}, 
									{
										valueField : "creditoRiesgoImp",
										name : "Riesgo",
										color : "#FF8633"
									},
									{
										valueField : "creditosVencidosImp",
										name : "Vencido",
										color : "#FF5E33"
									},
									{
										valueField : "creditoIncobrableImp",
										name : "Incobrable",
										color : "#cc3f44"
									}, 
									/*
									{
										axis: "capital",
										type: "spline",
										valueField : "carteraCapital",
										name : "Capital",
										color : "#008fd8"
									} */
									
									],
							valueAxis:[{
								grid: {visible:true}
							},
							{
								name: "capital",
								position: "right",
								grid: {visible: true},
								title: {
									text: "Cartera total"
								},
								label: {format: "largeNumber"}
							}],
							tooltip : {
								enabled : true,
								shared: true,
								format: {type: "largeNumber",
										precision: 1},
								customizeTooltip : function(arg){
									var items = arg.valueText.split("\n"),
									color = arg.point.getColor();
									$.each(items, function(index, item){
										if (item.indexOf(arg.seriesName)== 0){
											items[index] = $("<b>")
											.text(item)
											.css("color", color)
											.prop("outerHTML");
										}
									});
									return {text: items.join("\n")};
								}
							},
							title : "Reporte de la semana",
							legend : {
								verticalAlignment : "bottom",
								horizontalAlignment : "center"
							},
							"export" : {enabled:true},
							commonPaneSettings : {
								border : {
									visible : true,
									right : false
								}
							}
						});

    			
    			
    			// Requests per second gauge
    			$('#normalidad').dxCircularGauge({
    				scale: {
    					startValue: 0,
    					endValue: 100,
    					majorTick: {
    						tickInterval: 50
    					}
    				},
    				rangeContainer: {
    					palette: 'pastel',
    					width: 3,
    					ranges: [
    						{
    							startValue: 0,
    							endValue: 80,
    							color: gaugesPalette[0]
    						}, {
    							startValue: 80,
    							endValue: 85,
    							color: gaugesPalette[1]
    						}, {
    							startValue: 85,
    							endValue: 90,
    							color: gaugesPalette[2]
    						}, {
    							startValue: 90,
    							endValue: 100,
    							color: gaugesPalette[3]
    						}
    					],
    				},
    				value: 75,
    				valueIndicator: {
    					offset: 10,
    					color: '#2c2e2f',
    					spindleSize: 12
    				}
    			});

    			// Requests per second chart
    			$("#normalidad-chart").dxChart({
    				dataSource: normalidad_data,
    				commonPaneSettings: {
    					border: {
    						visible: true,
    						color: '#f5f5f5'
    					}
    				},
    				commonSeriesSettings: {
    					type: "area",
    					argumentField: "time",
    					border: {
    						color: '#68b828',
    						width: 1,
    						visible: true
    					}
    				},
    				series: [
    					{ valueField: "norms", name: "norms per Second", color: '#68b828', opacity: .5 },
    				],
    				commonAxisSettings: {
    					label: {
    						visible: true
    					},
    					grid: {
    						visible: true,
    						color: '#f5f5f5'
    					}
    				},
    				argumentAxis: {
    					valueMarginsEnabled: false,
    					label: {
    						customizeText: function (arg) {
    							return date('Y-m-d', arg.value);
    						}
    					},
    				},
    				legend: {
    					visible: false
    				}
    			});

    			
    			// CPU ind-cobranza
    			$('#ind-cobranza').dxCircularGauge({
    				scale: {
    					startValue: 0,
    					endValue: 150,
    					majorTick: {
    						tickInterval: 25
    					}
    				},
    				rangeContainer: {
    					palette: 'pastel',
    					width: 3,
    					ranges: [
    						{
    							startValue: 0,
    							endValue: 80,
    							color: gaugesPalette[0]
    						}, {
    							startValue: 80,
    							endValue: 90,
    							color: gaugesPalette[1]
    						}, {
    							startValue: 90,
    							endValue: 100,
    							color: gaugesPalette[2]
    						}, {
    							startValue: 100,
    							endValue: 150,
    							color: gaugesPalette[3]
    						}
    					],
    				},
    				value: 20,
    				valueIndicator: {
    					offset: 10,
    					color: '#2c2e2f',
    					spindleSize: 12
    				}
    			});

    			// CPU Usage chart
    			$("#ind-cobranza-chart").dxChart({
    				dataSource: cobranza_data,
    				commonPaneSettings: {
    					border: {
    						visible: true,
    						color: '#f5f5f5'
    					}
    				},
    				commonSeriesSettings: {
    					type: "area",
    					argumentField: "time",
    					border: {
    						color: '#7c38bc',
    						width: 1,
    						visible: true
    					}
    				},
    				series: [
    					{ valueField: "cobs", name: "Capacity used", color: '#7c38bc', opacity: .5 },
    				],
    				commonAxisSettings: {
    					label: {
    						visible: true
    					},
    					grid: {
    						visible: true,
    						color: '#f5f5f5'
    					}
    				},
    				argumentAxis: {
    					valueMarginsEnabled: false,
    					label: {
    						customizeText: function (arg) {
    							return date('Y-m-d', arg.value);
    						}
    					},
    				},
    				legend: {
    					visible: false
    				}
    			});


    			// Combine charts for filtering, grouped by time
    			var all_data_sources = [];
    			
    			$.map(normalidad_data, function(arg, i)
    			{
    				all_data_sources.push({
    					time: 					arg.time,
    					normalidadDia: 		normalidad_data[i].norms,
    					cobsDia: 				cobranza_data[i].cobs,
    				});
    			});


    			// Range Filter
    			$("#range-chart").dxRangeSelector({
    				dataSource: all_data_sources,
    				size: {
    					height: 140
    				},
    				chart: {
    					series: [
    						{ argumentField: "time", valueField: "normalidadDia", color: '#68b828', opacity: .65 },
    						{ argumentField: "time", valueField: "cobsDia", color: '#7c38bc', opacity: .65 },
    					]
    				},
    				selectedRange: {
    					startValue: all_data_sources[0].time,
    					endValue: all_data_sources[1].time
    				},
    				selectedRangeChanged: function(e)
    				{
    					var filter = {
    						normalidadDiaData: [],
    						cobsData: []
    					};
    					
    					$.map(all_data_sources, function(arg, i)
    					{
    						if(date("U", e.startValue) <= date("U", arg.time) && date("U", e.endValue) >= date("U", arg.time))
    						{
    							filter.normalidadDiaData.push({
    								time: arg.time,
    								norms: arg.normalidadDia
    							});
    							
    							filter.cobsData.push({
    								time: arg.time,
    								cobs: arg.cobsDia
    							});
    							
    						}
    					});
    					
    					$('#normalidad-chart').dxChart('instance').option('dataSource', filter.normalidadDiaData);
    					$('#ind-cobranza-chart').dxChart('instance').option('dataSource', filter.cobsData);
    				}
    			});



    			// Resize charts
    			$(window).on('xenon.resize', function()
    			{
    				$("#range-chart").data("dxRangeSelector").render();
    				
    				$("#normalidad-chart").data("dxChart").render();
    				$("#ind-cobranza-chart").data("dxChart").render();
    				
    				$("#normalidad").data("dxCircularGauge").render();
    				$("#ind-cobranza").data("dxCircularGauge").render();
    			});
    		}
    
    

});
